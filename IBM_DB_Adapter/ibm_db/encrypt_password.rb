require 'active_record/connection_adapters/ibm_db_password'

puts "Input: "
ARGV.each do|a|
  puts "  Argument: #{a}"
end

puts "Output: "
if (ARGV.count < 1)
  puts "Syntax:"
  puts "  ruby encrypt_pasword.rb password "
  puts "    password - user profile password to encrypt"
  exit(-1)
end

$password = ARGV[0]
if (ARGV.count > 1)
  $yaml_file = ARGV[1]
end
if (ARGV.count > 2)
  $yaml_level = ARGV[2]
end

$answer =
    ActiveXMLService::Base.generate_encoded_password(
      :yaml=>$yaml_file,
      :level=>$yaml_level,
      :password => $password
    )

puts "  pwd_enc: #{$answer}"
puts "  Config:  #{$yaml_file} level: #{$yaml_level}"

