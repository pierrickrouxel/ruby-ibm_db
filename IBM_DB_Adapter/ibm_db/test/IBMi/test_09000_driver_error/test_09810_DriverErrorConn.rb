# File: test_09810_DriverErrorConn.rb
require "../test_authorization/auth"
require "test/unit"

class Test_09810_DriverErrorConn < Test::Unit::TestCase

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0001_error_connect_bad_user_bad_password
    begin
      conn = IBM_DB::connect($db, "notuser", "invpass")
    rescue => msg
      conn = nil
    end
    assert_nil( conn, "conn not nil")  
  end

  def test_0002_error_connect_bad_password
    begin
      conn = IBM_DB::connect($db, $user, "invpass")
    rescue => msg
      conn = nil
    end
    assert_nil( conn, "conn not nil")  
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
