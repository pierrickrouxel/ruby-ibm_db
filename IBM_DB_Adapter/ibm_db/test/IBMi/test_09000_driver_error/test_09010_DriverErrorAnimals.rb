# File: test_09010_DriverErrorAnimals.rb.rb
require "../test_authorization/auth"
require "../test_data/animal"
require "test/unit"

class Test_09010_DriverErrorAnimals < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_error_select_result_bad_name_bad_index
    idx = 0
    stmt = IBM_DB::exec(@@conn,'SELECT id, breed, name, weight FROM animals WHERE id = 0')
    while IBM_DB::fetch_row(stmt)
      id = IBM_DB::result( stmt, 0 );
      assert_equal( $animals[idx][0], id, "ID mismatch")
      breed = IBM_DB::result( stmt, 1 );
      assert_match( $animals[idx][1], breed, "BREED mismatch")
      name = IBM_DB::result( stmt, "NAME" );
      assert_match( $animals[idx][2], name, "NAME mismatch")
      weight = IBM_DB::result( stmt, "WEIGHT" );
      assert_equal( $animals[idx][3], weight.to_f(), "WEIGHT mismatch")
      # invalid
      passport = IBM_DB::result( stmt, "PASSPORT" );
      assert_nil( passport, "PASSPORT not nil")
      # invalid
      fiddle = IBM_DB::result( stmt, 9 );
      assert_nil( fiddle, "column 9 not nil")
      puts "#{idx} #{id} #{breed} #{name} #{weight}"
      idx += 1
    end
    count = 1
    assert_equal(count, idx, "missing row data")
  end

  def test_0002_error_select_scrollable_negative_row
    idx = 2
    opts = 
    {
      IBM_DB::SQL_ATTR_CURSOR_SCROLLABLE=>IBM_DB::SQL_CURSOR_SCROLLABLE_ON,
      IBM_DB::SQL_ATTR_FOR_FETCH_ONLY=>IBM_DB::SQL_FETCH_ON
    }
    stmt = IBM_DB::prepare(@@conn,'SELECT * FROM animals WHERE weight < 10.0',opts)
    ret = IBM_DB::execute(stmt)
    begin
      row = IBM_DB::fetch_array(stmt,-1)
    rescue => msg
    end  
    assert_nil( row, "row not nil")
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
