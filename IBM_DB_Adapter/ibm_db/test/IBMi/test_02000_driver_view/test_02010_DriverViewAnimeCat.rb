# File: test_02010_DriverViewAnimeCat.rb
require "../test_authorization/auth"
require "../test_data/animal"
require "test/unit"

class Test_02010_DriverViewAnimeCat < Test::Unit::TestCase
  @@conn = nil
  @@anime = [ $animals[0] ]

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_view
    begin
      stmt = IBM_DB::exec(@@conn,'DROP VIEW ANIME_CAT')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE VIEW ANIME_CAT AS SELECT NAME, BREED FROM ANIMALS WHERE ID=0')
  end

  def test_0003_select_view
    idx = 0
    stmt = IBM_DB::exec(@@conn,'select * from ANIME_CAT')
    while row = IBM_DB::fetch_array(stmt)
      assert_match( @@anime[idx][2], row[0], "NAME mismatch")
      assert_match( @@anime[idx][1], row[1], "BREED mismatch")
      idx += 1
    end
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
