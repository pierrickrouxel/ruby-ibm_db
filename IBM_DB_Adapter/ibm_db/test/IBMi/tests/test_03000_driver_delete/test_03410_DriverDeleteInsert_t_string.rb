# File: test_03410_DriverDeleteInsert_t_string.rb
require "../test_authorization/auth"
require "test/unit"

class Test_03410_DriverDeleteInsert_t_string < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
    opts = {IBM_DB::SQL_ATTR_AUTOCOMMIT => IBM_DB::SQL_AUTOCOMMIT_OFF}
	IBM_DB::set_option(@@conn,opts,1)
  end

  def test_0002_delete
    stmt = IBM_DB::exec(@@conn,"insert into t_string values(123,1.222333,'one to one')")
    nbr = IBM_DB::num_fields(stmt)
    assert_equal(0,nbr,"mismatch nbr fields result set")
    nbr = IBM_DB::num_rows(stmt)
    assert_equal(1,nbr,"mismatch nbr rows affected")

    stmt = IBM_DB::exec(@@conn,'delete from t_string where a=123')
    nbr = IBM_DB::num_fields(stmt)
    assert_equal(0,nbr,"mismatch nbr fields result set")
    nbr = IBM_DB::num_rows(stmt)
    assert_equal(1,nbr,"mismatch nbr rows affected")

    IBM_DB::rollback(@@conn)
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
