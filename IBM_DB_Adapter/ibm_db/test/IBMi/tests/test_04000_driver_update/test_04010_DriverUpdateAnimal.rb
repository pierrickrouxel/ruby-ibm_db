# File: test_04010_DriverUpdateAnimal.rb
require "../test_authorization/auth"
require "../test_data/animal"
require "test/unit"

class Test_04010_DriverUpdateAnimal < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
    opts = {IBM_DB::SQL_ATTR_AUTOCOMMIT => IBM_DB::SQL_AUTOCOMMIT_OFF}
	IBM_DB::set_option(@@conn,opts,1)
  end

  def test_0002_update
    # start count
    stmt = IBM_DB::exec(@@conn,'SELECT count(*) FROM animals')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(7,row[0],"mismatch count after rollback")

    # update animal(s) matching criteria
    stmt = IBM_DB::exec(@@conn,'UPDATE animals SET id = 9 where id < 9')
    nbr = IBM_DB::num_rows(stmt)
    assert_equal(7,nbr,"mismatch nbr rows affected")

    # rollback to restore original data
    IBM_DB::rollback(@@conn)
    stmt = IBM_DB::exec(@@conn,'select count(id) from ANIMALS where id = 9')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(0,row[0],"mismatch count after rollback")
  end

  def test_0003_insert
    # start count
    stmt = IBM_DB::exec(@@conn,'SELECT count(*) FROM animals')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(7,row[0],"mismatch count after rollback")

    # insert a new animal
    stmt = IBM_DB::exec(@@conn,"INSERT INTO animals values (7,'bug','Brain Bug',10000.1)")
    nbr = IBM_DB::num_rows(stmt)
    assert_equal(1,nbr,"mismatch nbr rows affected")
    stmt = IBM_DB::exec(@@conn,'select count(*) from ANIMALS')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(8,row[0],"mismatch count after insert")

    # rollback to restore original data
    IBM_DB::rollback(@@conn)
    stmt = IBM_DB::exec(@@conn,'select count(*) from ANIMALS')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(7,row[0],"mismatch count after rollback")
  end

  def test_0004_insert_null
    # start count
    stmt = IBM_DB::exec(@@conn,'SELECT count(*) FROM animals')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(7,row[0],"mismatch count after rollback")

    # insert a new animal
    stmt = IBM_DB::prepare(@@conn, "INSERT INTO animals (id, breed, name, weight) VALUES (?, ?, ?, ?)")
    ret = IBM_DB::execute(stmt, [nil,'ghost',nil,nil])
    nbr = IBM_DB::num_rows(stmt)
    assert_equal(1,nbr,"mismatch nbr rows affected")
    stmt = IBM_DB::exec(@@conn,'SELECT id, breed, name, weight FROM animals WHERE weight IS NULL')
    row = IBM_DB::fetch_array(stmt)
    assert_nil( row[0], "ID mismatch")
    assert_match( 'ghost', row[1], "BREED mismatch")
    assert_nil( row[2], "NAME mismatch")
    assert_nil( row[3], "WEIGHT mismatch")

    # rollback to restore original data
    IBM_DB::rollback(@@conn)
    stmt = IBM_DB::exec(@@conn,'select count(*) from ANIMALS')
    row = IBM_DB::fetch_array(stmt)
    assert_equal(7,row[0],"mismatch count after rollback")
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
