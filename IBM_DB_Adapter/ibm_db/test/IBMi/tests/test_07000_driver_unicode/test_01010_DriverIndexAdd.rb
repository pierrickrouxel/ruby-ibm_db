# encoding: UTF-8
# File: test_01010_DriverIndexAdd.rb
require "../test_authorization/auth"
require "test/unit"

class Test_01010_DriverIndexAdd < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # export PATH=/PowerRuby/prV2R0M0/bin:$PATH
  # cd ./test_10000_driver_index
  # rake test TEST=test_01010_DriverIndexAdd.rb
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE ADD_INDEX_TEST')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE ADD_INDEX_TEST (COL1 VARCHAR(25)DEFAULT NULL)')

    stmt = IBM_DB::prepare(@@conn,'INSERT INTO ADD_INDEX_TEST (COL1) VALUES (?)')
    col1 = "something"
    ret = IBM_DB::execute(stmt,[col1])

    stmt = IBM_DB::prepare(@@conn,'CREATE INDEX INDX1 ON ADD_INDEX_TEST (COL1)')
    ret = IBM_DB::execute(stmt)
  end

end
