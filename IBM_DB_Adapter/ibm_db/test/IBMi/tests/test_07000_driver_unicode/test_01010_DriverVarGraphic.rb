# encoding: UTF-8
# File: test_01010_DriverVarGraphic.rb
require "../test_authorization/auth"
require "test/unit"

class Test_01010_DriverVarGraphic < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # export PATH=/PowerRuby/prV2R0M0/bin:$PATH
  # cd ./test_07000_driver_unicode
  # rake test TEST=test_01010_DriverVarGraphic.rb
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE VAR_GRAPHIC_TEST')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE VAR_GRAPHIC_TEST (COL1 VARGRAPHIC(25) CCSID 1200 DEFAULT NULL,'\
                                                              'COL2 GRAPHIC(25) CCSID 1200 DEFAULT NULL)')

    stmt = IBM_DB::prepare(@@conn,'INSERT INTO VAR_GRAPHIC_TEST (COL1,COL2) VALUES (?,?)')
    col1 = "something"
    col2 = "something"
    ret = IBM_DB::execute(stmt,[col1,col2])

    stmt = IBM_DB::exec(@@conn,'select * from VAR_GRAPHIC_TEST')
    all = Array.new
    while row = IBM_DB::fetch_array(stmt)
      all << row
      puts "\ncol1_before: |#{col1}|"
      puts " row0_after: |#{row[0]}|"
      puts " col1_class: #{col1.class.name}"
      puts " row0_class: #{row[0].class.name}"
      puts " col1_encode: #{col1.encoding}"
      puts " row0_encode: #{row[0].encoding}"
      puts " equal:#{col1==row[0]}"
      puts "col2_before: |#{col2}|"
      puts " col2_after: |#{row[1]}|"
      puts " col2_class: #{col2.class.name}"
      puts " row1_class: #{row[1].class.name}"
      puts " equal:#{col2==row[1]}"
      puts " col2_encode: #{col2.encoding}"
      puts " row1_encode: #{row[1].encoding}"
    end
    assert_equal(col1.encoding, all[0][0].encoding, "COL1 sent/received encoding not the same") 
    assert_equal(1, all.count, "Expecting 1 row and received #{all.count}")
    assert_equal(col1, all[0][0], "COL1 sent/received not the same")
    assert_equal(col2, all[0][1], "COL2 sent/received not the same") 
    
  end

end
