# File: test_06090_DriverFetchAnimalPicsFile.rb
require "../test_authorization/auth"
require "../test_data/animal"
require "test/unit"

class Test_06090_DriverFetchAnimalPicsFile < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0011_select_fetch_array
    idx = 0
    stmt = IBM_DB::exec(@@conn,'select * from ANIMAL_PICS_FILE')
    while row = IBM_DB::fetch_array(stmt)
      assert_match( $animal_pics[idx][0], row[0], "NAME mismatch")
      picture = File.dirname(File.realpath(__FILE__)) + "/../test_artifacts/#{$animal_pics[idx][1]}"
      size = File.size(picture)
      data = IO.binread(picture,size)
      assert_not_nil( data, "mismatch binread data is null #{picture} #{size}")
      hexdata = IBMDBPassword::Encrypt.encode(data)
      assert_not_nil( row[1], "mismatch fetch is null #{picture}")
      hexlob = IBMDBPassword::Encrypt.encode(row[1])
      assert_equal(hexdata, hexlob, "PICTURE mismatch")
      idx += 1
    end
  end

end
