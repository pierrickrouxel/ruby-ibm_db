# File: test_06010_DriverFetchAnimals.rb
require "../test_authorization/auth"
require "../test_data/animal"
require "../test_data/rowcol"

class Test_06010_DriverFetchAnimals < RowColUnitTest
  @@conn = nil
  @@row_all = [0,1,2,3,4,5,6]
  @@col_all = [0,1,2,3]
  @@row_weight_below_10 = [0,3,4,5]
  @@row_weight_above_300 = [2]
  @@row_id_equal_0 = [0]

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------
  def setup()
    self.init($animal_col, $animals, @@row_all, @@col_all)
  end

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0011_select_fetch_array
    stmt = IBM_DB::exec(@@conn,'select * from ANIMALS')
    all = Array.new
    while row = IBM_DB::fetch_array(stmt)
      all << row
    end
    self.animal_check_array(__method__, all)
  end

  def test_0012_select_fetch_assoc
    stmt = IBM_DB::exec(@@conn,'select * from ANIMALS')
    all = Array.new
    while row = IBM_DB::fetch_assoc(stmt)
      all << row
    end
    self.animal_check_assoc(__method__, all)
  end

  def test_0013_select_fetch_both
    stmt = IBM_DB::exec(@@conn,'select * from ANIMALS')
    all = Array.new
    while row = IBM_DB::fetch_both(stmt)
      all << row
    end
    self.animal_check_both(__method__, all)
  end

  def test_0014_select_fetch_object
    stmt = IBM_DB::exec(@@conn,'select * from ANIMALS')
    all = Array.new
    while row = IBM_DB::fetch_object(stmt)
      puts row
      all << row
    end
    self.animal_check_object(__method__, all)
  end


  def test_0015_select_fetch_row_array
    i = 0;
    stmt = IBM_DB::exec(@@conn,'SELECT * FROM animals')
    all = Array.new
    while IBM_DB::fetch_row(stmt)
      all[i] = Array.new
      for j in 0..self.cnt_col_all(1)
        all[i][j] = IBM_DB::result( stmt, j );
      end
      i += 1
    end
    self.animal_check_array(__method__, all)
  end


  def test_0016_select_fetch_row_assoc
    i = 0;
    stmt = IBM_DB::exec(@@conn,'SELECT * FROM animals')
    all = Array.new
    while IBM_DB::fetch_row(stmt)
      all[i] = Hash.new
      for j in 0..self.cnt_col_all(1)
        c = self.col_name(j)
        all[i][c] = IBM_DB::result( stmt, c.upcase );
      end
      i += 1
    end
    self.animal_check_assoc(__method__, all)
  end

  def test_0101_select_forward
    opts = 
    {
      IBM_DB::SQL_ATTR_CURSOR_TYPE=>IBM_DB::SQL_CURSOR_FORWARD_ONLY,
      IBM_DB::SQL_ATTR_CURSOR_SENSITIVITY => IBM_DB::SQL_UNSPECIFIED
    }
    stmt = IBM_DB::prepare(@@conn,'SELECT * FROM animals WHERE weight > 300.0',opts)
    ret = IBM_DB::execute(stmt)
    all = Array.new
    while row = IBM_DB::fetch_array(stmt)
      all << row
    end
    self.animal_check_array(__method__, all, @@row_weight_above_300)
  end

  def test_0102_select_static
    opts = 
    {
      IBM_DB::SQL_ATTR_CURSOR_TYPE=>IBM_DB::SQL_CURSOR_STATIC,
      IBM_DB::SQL_ATTR_CURSOR_SENSITIVITY => IBM_DB::SQL_INSENSITIVE
    }
    stmt = IBM_DB::prepare(@@conn,'SELECT * FROM animals WHERE weight > 300.0',opts)
    ret = IBM_DB::execute(stmt)
    all = Array.new
    while row = IBM_DB::fetch_array(stmt)
      all << row
    end
    self.animal_check_array(__method__, all, @@row_weight_above_300)
  end

  def test_0103_select_dynamic
    opts = 
    {
      IBM_DB::SQL_ATTR_CURSOR_TYPE=>IBM_DB::SQL_CURSOR_DYNAMIC
    }
    stmt = IBM_DB::prepare(@@conn,'SELECT * FROM animals WHERE weight > 300.0',opts)
    ret = IBM_DB::execute(stmt)
    all = Array.new
    while row = IBM_DB::fetch_array(stmt)
      all << row
    end
    self.animal_check_array(__method__, all, @@row_weight_above_300)
  end

  def test_0104_select_keyset
    opts = 
    {
      IBM_DB::SQL_ATTR_CURSOR_TYPE=>IBM_DB::SQL_CURSOR_KEYSET_DRIVEN,
      IBM_DB::SQL_ATTR_CURSOR_SENSITIVITY => IBM_DB::SQL_UNSPECIFIED
    }
    stmt = IBM_DB::prepare(@@conn,'SELECT * FROM animals WHERE weight > 300.0',opts)
    ret = IBM_DB::execute(stmt)
    all = Array.new
    while row = IBM_DB::fetch_array(stmt)
      all << row
    end
    self.animal_check_array(__method__, all, @@row_weight_above_300)
  end

  def test_0201_select_scrollable
    opts = 
    {
      IBM_DB::SQL_ATTR_CURSOR_SCROLLABLE=>IBM_DB::SQL_CURSOR_SCROLLABLE_ON,
      IBM_DB::SQL_ATTR_FOR_FETCH_ONLY=>IBM_DB::SQL_FETCH_ON
    }
    stmt = IBM_DB::prepare(@@conn,'SELECT * FROM animals WHERE weight < 10.0',opts)
    ret = IBM_DB::execute(stmt)
    all = Array.new
    for idx in 1..self.cnt_row_weight_below_10
      row = IBM_DB::fetch_array(stmt,idx)
      all << row
    end
    self.animal_check_array(__method__, all, @@row_weight_below_10)
  end

  def test_0202_select_static
    opts = 
    {
      IBM_DB::SQL_ATTR_CURSOR_TYPE=>IBM_DB::SQL_CURSOR_STATIC,
      IBM_DB::SQL_ATTR_FOR_FETCH_ONLY=>IBM_DB::SQL_FETCH_ON
    }
    stmt = IBM_DB::prepare(@@conn,'SELECT * FROM animals WHERE weight < 10.0',opts)
    ret = IBM_DB::execute(stmt)
    all = Array.new
    for idx in 1..self.cnt_row_weight_below_10
      row = IBM_DB::fetch_array(stmt,idx)
      all << row
    end
    self.animal_check_array(__method__, all, @@row_weight_below_10)
  end

  def test_0203_select_dynamic
    opts = 
    {
      IBM_DB::SQL_ATTR_CURSOR_TYPE=>IBM_DB::SQL_CURSOR_DYNAMIC,
      IBM_DB::SQL_ATTR_FOR_FETCH_ONLY=>IBM_DB::SQL_FETCH_ON
    }
    stmt = IBM_DB::prepare(@@conn,'SELECT * FROM animals WHERE weight < 10.0',opts)
    ret = IBM_DB::execute(stmt)
    all = Array.new
    for idx in 1..self.cnt_row_weight_below_10
      row = IBM_DB::fetch_array(stmt,idx)
      all << row
    end
    self.animal_check_array(__method__, all, @@row_weight_below_10)
  end

  def test_0204_select_keyset
    opts = 
    {
      IBM_DB::SQL_ATTR_CURSOR_TYPE=>IBM_DB::SQL_CURSOR_KEYSET_DRIVEN,
      IBM_DB::SQL_ATTR_FOR_FETCH_ONLY=>IBM_DB::SQL_FETCH_ON
    }
    stmt = IBM_DB::prepare(@@conn,'SELECT * FROM animals WHERE weight < 10.0',opts)
    ret = IBM_DB::execute(stmt)
    all = Array.new
    for idx in 1..self.cnt_row_weight_below_10
      row = IBM_DB::fetch_array(stmt,idx)
      all << row
    end
    self.animal_check_array(__method__, all, @@row_weight_below_10)
  end

  def test_0303_fetch_row_mixed_out_of_sequence
    stmt = IBM_DB::exec(@@conn,'SELECT id, breed, name, weight FROM animals WHERE id = 0')
    all = Array.new
    while IBM_DB::fetch_row(stmt)
      weight = IBM_DB::result( stmt, 3 );
      breed = IBM_DB::result( stmt, "BREED" );
      id = IBM_DB::result( stmt, "ID" );
      name = IBM_DB::result( stmt, 2 );
      all << [id, breed, name, weight]
    end
    self.animal_check_array(__method__, all, @@row_id_equal_0)
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

  def row_all()
    @@row_all
  end
  def cnt_row_all(start=0)
    @@row_all.count - start
  end

  def row_weight_below_10()
    @@row_weight_below_10
  end
  def cnt_row_weight_below_10(start=0)
    @@row_weight_below_10.count - start
  end

  def row_weight_above_300()
    @@row_weight_above_300
  end
  def cnt_row_weight_above_300(start=0)
    @@row_weight_above_300.count - start
  end

  def row_id_equal_0()
    @@row_id_equal_0
  end
  def cnt_row_id_equal_0(start=0)
    @@row_id_equal_0.count - start
  end

  def col_name(idx)
    $animal_col[idx]
  end

  def col_all()
    @@col_all
  end
  def cnt_col_all(start=0)
    @@col_all.count - start
  end

  def animal_check_array(test, all, row_exp = @@row_all , col_exp = @@col_all)
    self.rowcol_check_array(test, all, row_exp, col_exp)
  end
  def animal_check_assoc(test, all, row_exp = @@row_all , col_exp = @@col_all)
    self.rowcol_check_assoc(test, all, row_exp, col_exp)
  end
  def animal_check_both(test, all, row_exp = @@row_all , col_exp = @@col_all)
    self.rowcol_check_both(test, all, row_exp, col_exp)
  end
  def animal_check_object(test, all, row_exp = @@row_all , col_exp = @@col_all)
    # rows
    all1 = Array.new
    i_all = 0
    row_exp.each { |i|
      # cols
      all1[i_all] = Array.new()
      all1[i_all][0] = all[i_all].id    
      all1[i_all][1] = all[i_all].breed    
      all1[i_all][2] = all[i_all].name    
      all1[i_all][3] = all[i_all].weight    
      i_all += 1;
    }
    self.rowcol_check_array(test, all1, row_exp, col_exp)
  end

end
