# File: test_06160_DriverFetchProject.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_06160_DriverFetchProject < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0011_select_fetch_array
    idx = 0
    stmt = IBM_DB::exec(@@conn,'select * from PROJECT')
    while row = IBM_DB::fetch_array(stmt)
      puts "exp #{idx} #{$project[idx][0]} #{$project[idx][1]} #{$project[idx][2]} #{$project[idx][3]} #{$project[idx][4]} #{$project[idx][5]} #{$project[idx][6]} ( #{$project[idx][7]} )"
      puts "row #{idx} #{row[0]} #{row[1]} #{row[2]} #{row[3]} #{row[4]} #{row[5]} #{row[6]} ( #{row[7]} )"
      assert_match( $project[idx][0], row[0], "PROJNO mismatch")
      assert_match( $project[idx][1], row[1], "PROJNAME mismatch")
      assert_match( $project[idx][2], row[2], "DEPTNO mismatch")
      assert_match( $project[idx][3], row[3], "RESPEMP mismatch")
      assert_equal( $project[idx][4], row[4], "PRSTAFF mismatch")
      assert_match( $project[idx][5], row[5], "PRSTDATE mismatch")
      assert_match( $project[idx][6], row[6], "PRENDATE mismatch")
      if row[7] != nil
        assert_match( $project[idx][7], row[7], "MAJPROJ mismatch")
      else
        assert_equal( $project[idx][7], row[7], "MAJPROJ mismatch nil")
      end
      idx += 1
    end
    count = $project.count
    assert_equal(count, idx, "missing row data")
  end

end

