# File: test_01110_DriverPrepareDepartment.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_01110_DriverPrepareDepartment < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE DEPARTMENT')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE DEPARTMENT (DEPTNO CHAR(3) NOT NULL, DEPTNAME VARCHAR(29) NOT NULL, MGRNO CHAR(6), ADMRDEPT CHAR(3) NOT NULL, LOCATION CHAR(16))')
  end

  def test_0002_insert_table
    stmt = IBM_DB::prepare(@@conn,'INSERT INTO DEPARTMENT (DEPTNO, DEPTNAME, MGRNO, ADMRDEPT, LOCATION) VALUES (?, ?, ?, ?, ?)')
    $departments.each { |a|
     ret = IBM_DB::execute(stmt,a)
    }
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
