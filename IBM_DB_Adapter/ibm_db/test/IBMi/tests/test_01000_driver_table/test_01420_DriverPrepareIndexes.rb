# File: test_01420_DriverPrepareIndexes.rb
require "../test_authorization/auth"
require "test/unit"

class Test_01420_DriverPrepareIndexes < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table_index
    begin
      stmt = IBM_DB::exec(@@conn,'DROP INDEX index1')
    rescue => msg
    end  
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE index_test1')
    rescue => msg
    end  
    begin
      stmt = IBM_DB::exec(@@conn,'DROP INDEX index2')
    rescue => msg
    end  
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE index_test2')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE index_test1 (id INTEGER, data VARCHAR(50))')
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE index_test2 (id INTEGER, data VARCHAR(50))')
  end

  def test_0002_create_index
    stmt = IBM_DB::exec(@@conn,'CREATE INDEX index1 ON index_test1 (id)')
    stmt = IBM_DB::exec(@@conn,'CREATE INDEX index2 ON index_test2 (data)')
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
