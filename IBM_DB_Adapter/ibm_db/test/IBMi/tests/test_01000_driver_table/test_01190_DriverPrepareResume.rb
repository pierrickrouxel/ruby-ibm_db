# File: test_01190_DriverPrepareResume.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_01190_DriverPrepareResume < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE EMP_RESUME')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE EMP_RESUME (EMPNO CHAR(6) NOT NULL, RESUME_FORMAT VARCHAR(10) NOT NULL, RESUME CLOB(5K))')
  end

  def test_0002_insert_table
    stmt = IBM_DB::prepare(@@conn,'INSERT INTO EMP_RESUME (EMPNO, RESUME_FORMAT, RESUME) VALUES (?, ?, ?)')
    $resumes.each { |a| 
      name = a[0]
      form = a[1]
      lobname = a[2]
      resume = File.dirname(File.realpath(__FILE__)) + "/../test_artifacts/#{lobname}"
      puts resume
      size = File.size(resume)
      data = IO.read(resume,size)
      ret = IBM_DB::bind_param(stmt, 1, "name", IBM_DB::SQL_PARAM_INPUT)
      ret = IBM_DB::bind_param(stmt, 2, "form", IBM_DB::SQL_PARAM_INPUT)
      ret = IBM_DB::bind_param(stmt, 3, "data", IBM_DB::SQL_PARAM_INPUT)
      ret = IBM_DB::execute(stmt)
    }
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------
  
end
