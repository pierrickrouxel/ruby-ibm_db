# File: test_01010_DriverPrepareTableAnimals.rb
require "../test_authorization/auth"
require "../test_data/animal"
require "test/unit"

class Test_01010_DriverPrepareTableAnimals < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE ANIMALS')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE ANIMALS (ID INTEGER, BREED VARCHAR(32), NAME CHAR(16), WEIGHT DECIMAL(7,2))')
  end

  def test_0002_insert_table
    stmt = IBM_DB::prepare(@@conn,'INSERT INTO ANIMALS (ID, BREED, NAME, WEIGHT) VALUES (?, ?, ?, ?)')
    $animals.each { |a|
     ret = IBM_DB::execute(stmt,a)
    }
  end

  def test_003_create_proc
    assert_not_nil( @@conn, "1 exec connection nil failure")
    begin
      stmt = IBM_DB::exec(@@conn,'DROP PROCEDURE MATCH_ANIMAL')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,"CREATE PROCEDURE match_animal(IN first_name VARCHAR(128), INOUT second_name VARCHAR(128), OUT animal_weight DOUBLE) DYNAMIC RESULT SETS 1 LANGUAGE SQL BEGIN DECLARE match_name INT DEFAULT 0; DECLARE c1 CURSOR FOR SELECT COUNT(*) FROM animals WHERE name IN (second_name); DECLARE c2 CURSOR FOR SELECT SUM(weight) FROM animals WHERE name in (first_name, second_name); DECLARE c3 CURSOR WITH RETURN FOR SELECT name, breed, weight FROM animals WHERE name BETWEEN first_name AND second_name ORDER BY name; OPEN c1; FETCH c1 INTO match_name; IF (match_name > 0) THEN SET second_name = 'TRUE'; END IF; CLOSE c1; OPEN c2; FETCH c2 INTO animal_weight; CLOSE c2; OPEN c3; END")
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
