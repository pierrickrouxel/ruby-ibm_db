# File: test_01210_DriverPrepareT_string.rb
require "../test_authorization/auth"
require "test/unit"

class Test_01210_DriverPrepareT_string < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE t_string')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE T_STRING (A INTEGER, B DOUBLE, C VARCHAR(100))')
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
