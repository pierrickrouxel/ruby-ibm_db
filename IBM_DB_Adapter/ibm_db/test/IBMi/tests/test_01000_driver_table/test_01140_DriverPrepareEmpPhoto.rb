# File: test_01140_DriverPrepareEmpPhoto.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_01140_DriverPrepareEmpPhoto < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE EMP_PHOTO')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE EMP_PHOTO (EMPNO CHAR(6) NOT NULL, PHOTO_FORMAT VARCHAR(10) NOT NULL, PICTURE BLOB(100K), PRIMARY KEY(EMPNO, PHOTO_FORMAT))')
  end

  def test_0002_insert_table
    stmt = IBM_DB::prepare(@@conn,'INSERT INTO EMP_PHOTO (EMPNO, PHOTO_FORMAT, PICTURE) VALUES (?, ?, ?)')
    $employee_photos.each { |a| 
      empno = a[0]
      pictype = a[1]
      picname = a[2]
      picture = File.dirname(File.realpath(__FILE__)) + "/../test_artifacts/#{picname}"
      puts picture
      ret = IBM_DB::bind_param(stmt, 1, "empno", IBM_DB::SQL_PARAM_INPUT)
      ret = IBM_DB::bind_param(stmt, 2, "pictype", IBM_DB::SQL_PARAM_INPUT)
      ret = IBM_DB::bind_param(stmt, 3, "picture", IBM_DB::PARAM_FILE)
      ret = IBM_DB::execute(stmt)
    }
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
