# File: test_01310_DriverPrepareXT.rb
require "../test_authorization/auth"
require "test/unit"

class Test_01310_DriverPrepareXT < Test::Unit::TestCase
  @@conn = nil

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE xt1')
    rescue => msg
    end  
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE xt2')
    rescue => msg
    end  
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE xt3')
    rescue => msg
    end  
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE xt4')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE xt2( c1 integer, c2 varchar(40))')
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE xt1( c1 integer, c2 varchar(40))')
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE xt4( c1 integer, c2 varchar(40))')
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE xt3( c1 integer, c2 varchar(40))')
  end

end
