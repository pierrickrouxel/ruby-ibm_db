# File: test_01120_DriverPrepareEmpAct.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_01120_DriverPrepareEmpAct < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE EMP_ACT')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE EMP_ACT (EMPNO CHAR(6) NOT NULL, PROJNO CHAR(6) NOT NULL, ACTNO SMALLINT NOT NULL, EMPTIME DECIMAL(5,2), EMSTDATE DATE, EMENDATE DATE)')
  end

  def test_0002_insert_table
    stmt = IBM_DB::prepare(@@conn,'INSERT INTO EMP_ACT (EMPNO, PROJNO, ACTNO, EMPTIME, EMSTDATE, EMENDATE) VALUES (?, ?, ?, ?, ?, ?)')
    $employee_acts.each { |a|
     ret = IBM_DB::execute(stmt,a)
    }
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
