# File: test_16010_AdapterFetchAnimal.rb
# ActiveRecord::Base.establish_connection keywords (see auth_active)
require "../test_authorization/auth"
require "../test_data/animal"
require "../test_data/rowcol"
require 'rubygems'
require 'active_record'

class AnimalActiveRecord < ActiveRecord::Base
  self.table_name = "ANIMALS"
end

class Test_16010_AdapterFetchAnimal < RowColUnitTest
  @@col_all = [0,1,2,3]
  @@col_id_name = [0,2]
  @@col_id_breed = [0,1]
  @@col_name_weight = [2,3]
  @@row_all = [0,1,2,3,4,5,6]
  @@row_weight_below_10 = [0,3,4,5]
  @@row_weight_above_300 = [2]
  @@row_id_equal_0 = [0]
  @@row_limit_2 = [0,1]
  @@row_first = [0]
  @@row_last = [6]
  @@row_offset_3_limit_2 = [3,4]

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------
  def setup()
    self.init($animal_col, $animals, @@row_all, @@col_all)
  end

  def test_0010_select_fetch_all
    animals = AnimalActiveRecord.find(:all, :order => 'ID')
    all = Array.new
    animals.each { |row|
      all << [ 
               row.id, 
               row.breed, 
               row.name, 
               row.weight 
             ]
    }
    self.animal_check_array(__method__,all)
  end

  def test_0020_select_fetch_below_10
    animals = AnimalActiveRecord.find(:all, :order => 'ID', :conditions => "weight < 10.0")
    all = Array.new
    animals.each { |row|
      all << [ 
               row.id, 
               row.breed, 
               row.name, 
               row.weight 
             ]
    }
    self.animal_check_array(__method__,all,@@row_weight_below_10)
  end

  def test_0030_select_fetch_above_300
    animals = AnimalActiveRecord.find(:all, :order => 'ID', :conditions => "weight > 300.0")
    all = Array.new
    animals.each { |row|
      all << [ 
               row.id, 
               row.breed, 
               row.name, 
               row.weight 
             ]
    }
    self.animal_check_array(__method__,all,@@row_weight_above_300)
  end

  def test_0040_select_fetch_equal_0
    animals = AnimalActiveRecord.find(:all, :order => 'ID', :conditions => "id = 0")
    all = Array.new
    animals.each { |row|
      all << [ 
               row.id, 
               row.breed, 
               row.name, 
               row.weight 
             ]
    }
    self.animal_check_array(__method__,all,@@row_id_equal_0)
  end

  def test_0110_select_fetch_id_name
    animals = AnimalActiveRecord.find(:all, :order => 'ID', :select => "ID, NAME")
    all = Array.new
    animals.each { |row|
      all << [ 
               row.id, 
               row.name 
             ]
    }
    self.animal_check_array(__method__,all,@@row_all,@@col_id_name)
  end

  def test_0120_select_fetch_id_breed_limit_2
    animals = AnimalActiveRecord.find(:all, :order => 'ID', :select => "ID, BREED", :limit => 2)
    all = Array.new
    animals.each { |row|
      all << [ 
               row.id, 
               row.breed 
             ]
    }
    self.animal_check_array(__method__,all,@@row_limit_2,@@col_id_breed)
  end

  def test_0130_select_fetch_id_set_below_10
    animals = AnimalActiveRecord.find(@@row_weight_below_10)
    all = Array.new
    animals.each { |row|
      all << [ 
               row.id, 
               row.breed, 
               row.name, 
               row.weight 
             ]
    }
    self.animal_check_array(__method__,all,@@row_weight_below_10)
  end

  def test_0140_select_fetch_id_set_above_300
    animals = AnimalActiveRecord.find(@@row_weight_above_300)
    all = Array.new
    animals.each { |row|
      all << [ 
               row.id, 
               row.breed, 
               row.name, 
               row.weight 
             ]
    }
    self.animal_check_array(__method__,all,@@row_weight_above_300)
  end

  def test_0150_select_fetch_id_set_equal_0
    animals = AnimalActiveRecord.find(@@row_id_equal_0, :select => "NAME, WEIGHT")
    all = Array.new
    animals.each { |row|
      all << [ 
               row.name, 
               row.weight 
             ]
    }
    self.animal_check_array(__method__,all,@@row_id_equal_0,@@col_name_weight)
  end


  def missing_test_0160_select_fetch_first
    animals = AnimalActiveRecord.find(:first, :select => "NAME, WEIGHT")
    all = Array.new
    animals.each { |row|
      all << [ 
               row.name, 
               row.weight 
             ]
    }
    self.animal_check_array(__method__,all,@@row_first,@@col_name_weight)
  end

  def missing_test_0170_select_fetch_last
    animals = AnimalActiveRecord.find(:last, :select => "NAME, WEIGHT")
    all = Array.new
    animals.each { |row|
      all << [ 
               row.name, 
               row.weight 
             ]
    }
    self.animal_check_array(__method__,all,@@row_last,@@col_name_weight)
  end

  def test_0180_select_fetch_id_breed_offset_3_limit_2
    animals = AnimalActiveRecord.find(:all, :order => 'ID', :select => "ID, BREED", :offset => 3, :limit => 2)
    all = Array.new
    animals.each { |row|
      all << [ 
               row.id, 
               row.breed 
             ]
    }
    self.animal_check_array(__method__,all,@@row_offset_3_limit_2,@@col_id_breed)
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

  def animal_check_array(test, all, row_exp = @@row_all , col_exp = @@col_all)
    self.rowcol_check_array(test, all, row_exp, col_exp)
  end

end
