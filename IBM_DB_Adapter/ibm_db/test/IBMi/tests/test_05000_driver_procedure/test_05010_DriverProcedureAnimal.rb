# File: test_05010_DriverProcedureAnimal.rb
require "../test_authorization/auth"
require "../test_data/animal"
require "../test_data/rowcol"

class Test_05010_DriverProcedureAnimal < RowColUnitTest
  @@conn = nil
  @@row_all = [0,1,2,3,4,5,6]
  @@col_all = [0,1,2,3]
  @@row_between = [1,0,5,2,6]
  @@col_between = [2,1,3]

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------
  def setup()
    self.init($animal_col, $animals, @@row_all, @@col_all)
  end

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0010_proc_call
    stmt = IBM_DB::prepare(@@conn,'CALL MATCH_ANIMAL(?, ?, ?)')
    name1 = 'Peaches'
    name2 = 'Rickety Ride'
    weight = 22.22
    ret = IBM_DB::bind_param(stmt, 1, "name1", IBM_DB::SQL_PARAM_INPUT)
    ret = IBM_DB::bind_param(stmt, 2, "name2", IBM_DB::SQL_PARAM_INPUT_OUTPUT)
    ret = IBM_DB::bind_param(stmt, 3, "weight", IBM_DB::SQL_PARAM_OUTPUT)
    ret = IBM_DB::execute(stmt)
    
    assert_match( name1, "Peaches", "name1 mismatch")
    assert_match( name2, "TRUE", "name2 mismatch")
    assert_equal( weight, 12.3, "weight mismatch")

    all = Array.new
    while row = IBM_DB::fetch_array(stmt)
      all << row
    end
    self.animal_check_array(__method__, all, @@row_between, @@col_between)
    
    puts "Script ran clean #{name1} #{name2} #{weight}"
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

  def animal_check_array(test, all, row_exp = @@row_all , col_exp = @@col_all)
    self.rowcol_check_array(test, all, row_exp, col_exp)
  end

end

