# test data for various tables
require "test/unit"


class RowColUnitTest  < Test::Unit::TestCase
  @@root_row_data  = [0]
  @@root_row_index = [0]
  @@root_col_data  = [0]
  @@root_col_index = [0]

  def init(exp_col_data, exp_row_data, exp_col_index, exp_row_index)
    @@root_col_data  = exp_col_data
    @@root_row_data  = exp_row_data
    @@root_col_index = exp_col_index
    @@root_row_index = exp_row_index
  end

  def rowcol_check_data(test, exp_col, exp_data, act_data)
    @@root_col_data = [ exp_col ]
    @@root_row_data = [ [ exp_data ] ]
    all             = [ [ act_data ] ]
    self.rowcol_check_array(test, all)
  end

  def rowcol_check_ds(test, exp_col, exp_data, act_data)
    @@root_col_data = [ exp_col ]
    @@root_row_data = [ [ exp_data ] ]
    all             = [ [ act_data ] ]
    self.rowcol_check_array(test, all)
  end

  def rowcol_check_array(test, all, row_exp = @@root_row_index , col_exp = @@root_col_index)
    # report
    report_1 = false
    report_c = Array.new
    report_a = Array.new
    report_b = Array.new
    # rows
    exp_row = row_exp.count
    nbr_row = all.count
    puts "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] row count #{exp_row} == #{nbr_row}"
    assert_equal(exp_row, nbr_row, "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] row count mismatch: #{exp_row} == #{nbr_row}")
    i_all = 0
    row_exp.each { |i|
      # cols
      exp_col = col_exp.count
      nbr_col = all[i_all].count
      assert_equal(exp_col, nbr_col, "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] col count mismatch: #{exp_col} == #{nbr_col}")
      j_all = 0
      report_a[i_all] = Array.new
      report_b[i_all] = Array.new
      col_exp.each { |j|
        c = @@root_col_data[j]
        a = @@root_row_data[i][j]
        b = all[i_all][j_all]
        # verbose report
        if report_1 == false
          report_c[j_all] = c
        end
        report_a[i_all][j_all] = a
        report_b[i_all][j_all] = b
        # float/real/decimal/string,
        # you sir are a String/BigDecimal,
        # therefore you must float yourself, etc. ... 
        # Ruby perfection be my name (Borg),
        # perhaps yet another reason to use PHP
        case a
        when String
          case b
          when String
            # ok
          else
            b = b.to_s()
          end 
        when Integer
          case b
          when Integer
            # ok
          else
            b = b.to_i()
          end 
        when Float
          case b
          when Float
            # ok
          else
            b = b.to_f()
          end 
        else
        end 
        # make a check
        case a
        when String
          assert_match( a, b, "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] row: #{i} col: #{j} or #{c} mismatch: #{a} == #{b}")
        else
          assert_equal( a, b, "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] row: #{i} col: #{j} or #{c} mismatch: #{a} == #{b}")
        end

        j_all += 1;
      }
      report_1 = true
      i_all += 1;
    }
    self.verbose_row_col(test, report_c, report_a, report_b)
  end

  def rowcol_check_assoc(test, all, row_exp = @@root_row_index , col_exp = @@root_col_index)
    # rows
    exp_row = row_exp.count
    nbr_row = all.count
    puts "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] row count #{exp_row} == #{nbr_row}"
    assert_equal(exp_row, nbr_row, "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] row count mismatch: #{exp_row} == #{nbr_row}")
    all2 = Array.new
    i_all = 0
    row_exp.each { |i|
      # cols
      exp_col = col_exp.count
      nbr_col = all[i_all].count
      assert_equal(exp_col, nbr_col, "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] col count mismatch: #{exp_col} == #{nbr_col}")
      all2[i_all] = Array.new
      j_all = 0
      col_exp.each { |j|
        c = @@root_col_data[j]
        assert(all[i_all].has_key?(c), "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] col key mismatch: #{c}")
        all2[i_all][j_all] = all[i_all][c]    
        j_all += 1;
      }
      i_all += 1;
    }
    self.rowcol_check_array(test, all2, row_exp, col_exp)
  end


  def rowcol_check_both(test, all, row_exp = @@root_row_index , col_exp = @@root_col_index)
    # rows
    exp_row = row_exp.count
    nbr_row = all.count
    puts "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] row count #{exp_row} == #{nbr_row}"
    assert_equal(exp_row, nbr_row, "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] row count mismatch: #{exp_row} == #{nbr_row}")
    all1 = Array.new
    all2 = Array.new
    i_all = 0
    row_exp.each { |i|
      # cols
      exp_col = col_exp.count
      nbr_col = all[i_all].count / 2
      assert_equal(exp_col, nbr_col, "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] col count mismatch: #{exp_col} == #{nbr_col}")
      all1[i_all] = Array.new
      all2[i_all] = Hash.new
      j_all = 0
      col_exp.each { |j|
        c = @@root_col_data[j]
        assert(all[i_all].has_key?(c), "#{test} #{__method__} [#{__FILE__}:#{__LINE__}] col key mismatch: #{c}")
        all1[i_all][j_all] = all[i_all][j_all]    
        all2[i_all][c] = all[i_all][c]    
        j_all += 1;
      }
      i_all += 1;
    }
    self.rowcol_check_array(test, all1, row_exp, col_exp)
    self.rowcol_check_assoc(test, all2, row_exp, col_exp)
  end


  def verbose_row_col(test, col, exp, usr)
    puts "#{test} ====>"
    cnt_row = exp.count
    cnt_col = col.count
    # report expected
    puts "Expected data:"
    rpt = ""
    for j in 0..cnt_col-1
      rpt << " %20s " % [col[j]]
    end
    puts rpt
    for i in 0..cnt_row-1
      rpt = ""
      for j in 0..cnt_col-1
        rpt << "|%20s|" % [exp[i][j]]
      end
      puts rpt
    end
    # report actual
    puts "Acual data:"
    rpt = ""
    for j in 0..cnt_col-1
      rpt << " %20s " % [col[j]]
    end
    puts rpt
    for i in 0..cnt_row-1
      rpt = ""
      for j in 0..cnt_col-1
        rpt << "|%20s|" % [usr[i][j]]
      end
      puts rpt
    end
  end

end

