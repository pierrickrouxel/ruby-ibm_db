# test data for various tables
$animal_col = 
    ['id', 'breed', 'name', 'weight']
$animals = 
    [
      [0, 'cat',        'Pook',         3.2],
      [1, 'dog',        'Peaches',      12.3],
      [2, 'horse',      'Smarty',       350.0],
      [3, 'gold fish',  'Bubbles',      0.1],
      [4, 'budgerigar', 'Gizmo',        0.2],
      [5, 'goat',       'Rickety Ride', 9.7],
      [6, 'llama',      'Sweater',      150]
    ]
$animal_pics = 
    [
      ['Pook','Pook.png'],
      ['Peaches','Peaches.png'],
      ['Smarty','Smarty.png'],
      ['Bubbles','Bubbles.png'],
      ['Gizmo','Gizmo.png'],
      ['Rickety Ride','RicketyRide.png'],
      ['Sweater','Sweater.png'],
      ['Spook', 'spook.png'],
      ['Helmut', 'pic1.jpg']
    ]

