# File: test_08420_DriverStatisticsIndexes.rb
require "../test_authorization/auth"
require "test/unit"

class Test_08420_DriverStatisticsIndexes < Test::Unit::TestCase
  @@conn = nil
  @@index = 
   [
    "INDEX_TEST1",
    "INDEX_TEST2"
   ]

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0003_meta_indexes
    idx = 1
    # TABLE_CAT:        The catalog that contains the table. The value is NULL if this table does not have catalogs.
    # TABLE_SCHEM:      Name of the schema that contains the table.
    # TABLE_NAME:       Name of the table.
    # NON_UNIQUE:       An integer value representing whether the index prohibits unique values, or whether the row represents statistics on the table itself:
    # INDEX_QUALIFIER:  A string value representing the qualifier that would have to be prepended to INDEX_NAME to fully qualify the index.
    # INDEX_NAME:       A string representing the name of the index.
    # TYPE:             An integer value representing the type of information contained in this row of the result set:
    # ORDINAL_POSITION: The 1-indexed position of the column in the index. NULL if the row contains statistics information about the table itself.
    # COLUMN_NAME:      The name of the column in the index. NULL if the row contains statistics information about the table itself.
    # ASC_OR_DESC:      A if the column is sorted in ascending order, D if the column is sorted in descending order, NULL if the row contains statistics information about the table itself.
    # CARDINALITY:      If the row contains information about an index, this column contains an integer value representing the number of unique values in the index. If the row contains information about the table itself, this column contains an integer value representing the number of rows in the table.
    # PAGES:            If the row contains information about an index, this column contains an integer value representing the number of pages used to store the index. If the row contains information about the table itself, this column contains an integer value representing the number of pages used to store the table.
    # FILTER_CONDITION:	Always returns NULL.
    found = nil     
    @@index.each { |a| 
     puts "#{idx}: #{a}"
     stmt = IBM_DB::statistics(@@conn, nil,  $config[:username], "#{a}", 1)
     while row = IBM_DB::fetch_assoc(stmt)
       puts "#{idx}: #{row}"
       assert_match( $config[:username],row['table_schem'],"mismatch TABLE_SCHEM")
       assert_match("INDEX_TEST#{idx}",row['table_name'],"mismatch TABLE_NAME")
       if row['non_unique']
         found = true
         assert_equal(1,row['non_unique'],"mismatch NON_UNIQUE")
         assert_match( $config[:username],row['index_qualifier'],"mismatch INDEX_QUALIFIER")
         assert_match("INDEX#{idx}",row['index_name'],"mismatch INDEX_NAME")
         assert_equal(3,row['type'],"mismatch TYPE")
         assert_equal(1,row['ordinal_position'],"mismatch ORDINAL_POSITION")
         assert_match("A",row['asc_or_desc'],"mismatch ASC_OR_DESC")
       else
         idx += 1
       end
     end
    }
    assert_not_nil(found,"mismatch no records found #{ $config[:username]}")
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end

