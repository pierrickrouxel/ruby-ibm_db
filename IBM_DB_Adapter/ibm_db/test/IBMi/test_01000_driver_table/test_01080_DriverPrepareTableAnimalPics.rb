# File: test_01080_DriverPrepareTableAnimalPics.rb
require "../test_authorization/auth"
require "../test_data/animal"
require "test/unit"

class Test_01080_DriverPrepareTableAnimalPics < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE ANIMAL_PICS')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE ANIMAL_PICS (NAME VARCHAR(32), PICTURE BLOB(5M))')
  end

  def test_0002_insert_table
    stmt = IBM_DB::prepare(@@conn,'INSERT INTO ANIMAL_PICS (NAME, PICTURE) VALUES (?, ?)')
    $animal_pics.each { |a| 
      name = a[0]
      picname = a[1]
      picture = File.dirname(File.realpath(__FILE__)).gsub("test_01000_driver_table","") + "test_artifacts/#{picname}"
      puts "\n #{picture}"
      size = File.size(picture)
      data = IO.binread(picture,size)
      ret = IBM_DB::bind_param(stmt, 1, "name", IBM_DB::SQL_PARAM_INPUT)
      ret = IBM_DB::bind_param(stmt, 2, "data", IBM_DB::SQL_PARAM_INPUT)
      ret = IBM_DB::execute(stmt)
    }
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
