# File: test_01150_DriverPrepareOrg.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_01150_DriverPrepareOrg < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE ORG')
    rescue => msg
    end  
    stmt = IBM_DB::exec(@@conn,'CREATE TABLE ORG (DEPTNUMB SMALLINT NOT NULL, DEPTNAME VARCHAR(14), MANAGER SMALLINT, DIVISION VARCHAR(10), LOCATION VARCHAR(13))')
  end

  def test_0002_insert_table
    stmt = IBM_DB::prepare(@@conn,'INSERT INTO ORG (DEPTNUMB, DEPTNAME, MANAGER, DIVISION, LOCATION) VALUES (?, ?, ?, ?, ?)')
    $orgs.each { |a|
     ret = IBM_DB::execute(stmt,a)
    }
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
