require "../test_authorization/auth"

if $naming == "sys"
  ActiveRecord::Base.establish_connection(
    :adapter   => 'ibm_db',
    :database  => $db,
    :username  => $user,
    :password  => $pass,
    :ibm_i_naming => IBM_DB::SQL_SYSTEM_NAMING_ON,
    :ibm_i_libl   => $libl,
    :ibm_i_curlib => $curlib
    # or use yaml file ...
    # :yaml  => '../test_authorization/auth.yaml',
    # :level => "development"
  )
  ActiveXMLService::Base.establish_connection(
    :adapter   => 'ibm_db',
    :database  => $db,
    :username  => $user,
    :password  => $pass,
    :ibm_i_naming => IBM_DB::SQL_SYSTEM_NAMING_ON,
    :ibm_i_libl   => $libl,
    :ibm_i_curlib => $curlib,
    :connection => $toolkit[:connection],
    :install    => $toolkit[:install],
    :ctl        => $toolkit[:ctl],
    :ipc        => $toolkit[:ipc],
    :size       => $toolkit[:size],
    :head       => $toolkit[:head]
    # or use yaml file ...
    # :yaml  => '../test_authorization/auth.yaml',
    # :level => "development"
  )
else
  ActiveRecord::Base.establish_connection(
    :adapter   => 'ibm_db',
    :database  => $db,
    :username  => $user,
    :password  => $pass
  )
  ActiveXMLService::Base.establish_connection(
    :adapter   => 'ibm_db',
    :database  => $db,
    :username  => $user,
    :password  => $pass,
    :connection => $toolkit[:connection],
    :install    => $toolkit[:install],
    :ctl        => $toolkit[:ctl],
    :ipc        => $toolkit[:ipc],
    :size       => $toolkit[:size],
    :head       => $toolkit[:head]
    # or use yaml file ...
    # :yaml  => '../test_authorization/auth.yaml',
    # :level => "development"
  )
end

