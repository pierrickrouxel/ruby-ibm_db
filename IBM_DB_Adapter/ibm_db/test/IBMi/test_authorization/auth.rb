require 'active_record'

if !ENV['TEST_YAML']
  ENV['TEST_YAML'] = "../config.yml"
end
if !ENV['TEST_ENV']
  ENV['TEST_ENV'] = "development"
end

ActiveRecord::Base.establish_connection(
  :adapter   => 'ibm_db'
)

require 'active_record/connection_adapters/ibm_db_password'
$config = Hash.new
$config = IBMDBPassword::Encrypt.parse_yaml($config,ENV['TEST_YAML'],ENV['TEST_ENV'])
$config = IBMDBPassword::Encrypt.symbolize_keys($config)

