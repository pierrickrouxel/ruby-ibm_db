# File: test_14010_AdapterUpdateAnimal.rb
# ActiveRecord::Base.establish_connection keywords (see auth_active)
require "../test_authorization/auth_active"
require "../test_data/animal"
require "../test_data/rowcol"
require 'rubygems'
require 'active_record'

class AnimalActiveRecord < ActiveRecord::Base
  self.table_name = "ANIMALS"
end

class Test_14010_AdapterUpdateAnimal < RowColUnitTest
  @@col_all = [0,1,2,3]
  @@row_all = [0,1,2,3,4,5,6]

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------
  def setup()
    self.init($animal_col, $animals, @@row_all, @@col_all)
    @@auth.verbose_global()
  end

  def test_0010_update_all
    @@auth.verbose "begin transaction"
    AnimalActiveRecord.transaction do
      AnimalActiveRecord.update_all("weight=410.0", "id > 1")
      animals = AnimalActiveRecord.find(:all, :order => 'ID', :conditions => "weight > 400.0")
      nbr = animals.count
      @@auth.verbose "update all count = #{nbr}"
      assert_equal( 5, animals.count, "update all count mismatch")
      @@auth.verbose "rollback now"
      raise ActiveRecord::Rollback
    end
    @@auth.verbose "end transaction"
    self.animal_check_rollback(__method__)
  end


  def test_0020_update_even
    @@auth.verbose "begin transaction"
    AnimalActiveRecord.transaction do
      chicken =
      {
        0 => { "breed" => "chicken" },
        2 => { "breed" => "chicken" },
        4 => { "breed" => "chicken" },
        6 => { "breed" => "chicken" }
      }
      AnimalActiveRecord.update(chicken.keys, chicken.values)
      animals = AnimalActiveRecord.find(:all, :order => 'ID', :conditions => "breed = 'chicken'")
      nbr = animals.count
      @@auth.verbose "update even chicken count = #{nbr}"
      assert_equal( 4, animals.count, "update  count mismatch")
      @@auth.verbose "rollback now"
      raise ActiveRecord::Rollback
    end
    @@auth.verbose "end transaction"
    self.animal_check_rollback(__method__)
  end


  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------
  def animal_check_rollback(test)
    animals = AnimalActiveRecord.find(:all, :order => 'ID')
    all = Array.new
    animals.each { |row|
      all << [ 
             row.id, 
             row.breed, 
             row.name, 
             row.weight 
             ]
    }
    self.animal_check_array(test,all)
  end

  def animal_check_array(test, all, row_exp = @@row_all , col_exp = @@col_all)
    self.rowcol_check_array(test, all, row_exp, col_exp)
  end

end
