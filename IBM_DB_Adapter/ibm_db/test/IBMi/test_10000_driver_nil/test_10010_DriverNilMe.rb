# File: test_10010_DriverPrepareTableNilMe.rb
require "../test_authorization/auth"
require "test/unit"

class Test_10010_DriverNilMe < Test::Unit::TestCase
  @@conn = nil
  @@nilsmall  = nil
  @@nilint    = nil
  @@nilbig    = nil
  @@nildec    = nil
  @@nilnum    = nil
  @@nilflt    = nil
  @@nilreal   = nil
  @@nildbl    = nil
  @@nilchr    = nil
  @@nilvchr   = nil
  @@nilgph    = nil
  @@nilvgph   = nil
  @@nilbin    = nil
  @@nilvbin   = nil
  @@nildate   = nil
  @@niltime   = nil
  @@nilts     = nil
  @@nilclob   = nil
  @@nildbclob = nil
  @@nilblob   = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	  @@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE NilMe')
    rescue => msg
    end
    sql = "CREATE TABLE NilMe (" + 
          "nilsmall smallint," +
          "nilint integer," +
          "nilbig bigint," +
          "nildec decimal(12,4)," +
          "nilnum numeric(12,4)," +
          "nilflt float," +
          "nilreal real," +
          "nildbl double," +
          "nilchr char(12)," +
          "nilvchr varchar(12)," +
          "nilgph graphic(12)," +
          "nilvgph vargraphic(12)," +
          "nilbin binary(12)," +
          "nilvbin varbinary(12)," +
          "nildate date," +
          "niltime time," +
          "nilts timestamp," +
          "nilclob clob(1M)," +
          "nildbclob dbclob(1M)," +
          "nilblob blob(1M)" +
          ")"
    stmt = IBM_DB::exec(@@conn,sql)
  end

  def test_0002_insert_nil
    sql = "INSERT INTO NilMe (" +
          "nilsmall," +
          "nilint," +
          "nilbig," +
          "nildec," +
          "nilnum," +
          "nilflt," +
          "nilreal," +
          "nildbl," +
          "nilchr," +
          "nilvchr," +
          "nilgph," +
          "nilvgph," +
          "nilbin," +
          "nilvbin," +
          "nildate," +
          "niltime," +
          "nilts," +
          "nilclob," +
          "nildbclob," +
          "nilblob" +
          ")" +
           " VALUES (" +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?" +
          ")"
    stmt = IBM_DB::prepare(@@conn,sql)
    a = [
    @@nilsmall,
    @@nilint,
    @@nilbig,
    @@nildec,
    @@nilnum,
    @@nilflt,
    @@nilreal,
    @@nildbl,
    @@nilchr,
    @@nilvchr,
    @@nilgph,
    @@nilvgph,
    @@nilbin,
    @@nilvbin,
    @@nildate,
    @@niltime,
    @@nilts,
    @@nilclob,
    @@nildbclob,
    @@nilblob]
    ret = IBM_DB::execute(stmt,a)
  end

  def test_0003_select_nil
    stmt = IBM_DB::exec(@@conn,'select * from NilMe')
    all = Array.new
    r = IBM_DB::fetch_assoc(stmt)

    assert_equal("NilClass",r['nilsmall'].class.name, "nilbig    - NilClass mismatch #{r['nilsmall'].class.name}")
    assert_equal("NilClass",r['nilint'].class.name,   "nilint    - NilClass mismatch #{r['nilint'].class.name}")
    assert_equal("NilClass",r['nilbig'].class.name,   "nilbig    - NilClass mismatch #{r['nilbig'].class.name}")
    assert_equal("NilClass",r['nildec'].class.name,   "nildec    - NilClass mismatch #{r['nildec'].class.name}")
    assert_equal("NilClass",r['nilnum'].class.name,   "nilnum    - NilClass mismatch #{r['nilnum'].class.name}")
    assert_equal("NilClass",r['nilflt'].class.name,   "nilflt    - NilClass mismatch #{r['nilflt'].class.name}")
    assert_equal("NilClass",r['nilreal'].class.name,  "nilreal   - NilClass mismatch #{r['nilreal'].class.name}")
    assert_equal("NilClass",r['nildbl'].class.name,   "nildbl    - NilClass mismatch #{r['nildbl'].class.name}")
    assert_equal("NilClass",r['nilchr'].class.name,   "nilchr    - NilClass mismatch #{r['nilchr'].class.name}")
    assert_equal("NilClass",r['nilvchr'].class.name,  "nilvchr   - NilClass mismatch #{r['nilvchr'].class.name}")
    assert_equal("NilClass",r['nilgph'].class.name,   "nilgph    - NilClass mismatch #{r['nilgph'].class.name}")
    assert_equal("NilClass",r['nilvgph'].class.name,  "nilvgph   - NilClass mismatch #{r['nilvgph'].class.name}")
    assert_equal("NilClass",r['nilbin'].class.name,   "nilbin    - NilClass mismatch #{r['nilbin'].class.name}")
    assert_equal("NilClass",r['nilvbin'].class.name,  "nilvbin   - NilClass mismatch #{r['nilvbin'].class.name}")
    assert_equal("NilClass",r['nildate'].class.name,  "nildate   - NilClass mismatch #{r['nildate'].class.name}")
    assert_equal("NilClass",r['niltime'].class.name,  "niltime   - NilClass mismatch #{r['niltime'].class.name}")
    assert_equal("NilClass",r['nilts'].class.name,    "nilts     - NilClass mismatch #{r['nilts'].class.name}")
    assert_equal("NilClass",r['nilclob'].class.name,  "nilclob   - NilClass mismatch #{r['nilclob'].class.name}")
    assert_equal("NilClass",r['nildbclob'].class.name,"nildbclob - NilClass mismatch #{r['nildbclob'].class.name}")
    assert_equal("NilClass",r['nilblob'].class.name,  "nilblob   - NilClass mismatch #{r['nilblob'].class.name}")

    assert_equal(@@nilsmall,  r['nilsmall'], "nilbig    - nil mismatch #{r['nilsmall']}")
    assert_equal(@@nilint,    r['nilint'],   "nilint    - nil mismatch #{r['nilint']}")
    assert_equal(@@nilbig,    r['nilbig'],   "nilbig    - nil mismatch #{r['nilbig']}")
    assert_equal(@@nildec,    r['nildec'],   "nildec    - nil mismatch #{r['nildec']}")
    assert_equal(@@nilnum,    r['nilnum'],   "nilnum    - nil mismatch #{r['nilnum']}")
    assert_equal(@@nilflt,    r['nilflt'],   "nilflt    - nil mismatch #{r['nilflt']}")
    assert_equal(@@nilreal,   r['nilreal'],  "nilreal   - nil mismatch #{r['nilreal']}")
    assert_equal(@@nildbl,    r['nildbl'],   "nildbl    - nil mismatch #{r['nildbl']}")
    assert_equal(@@nilchr,    r['nilchr'],   "nilchr    - nil mismatch #{r['nilchr']}")
    assert_equal(@@nilvchr,   r['nilvchr'],  "nilvchr   - nil mismatch #{r['nilvchr']}")
    assert_equal(@@nilgph,    r['nilgph'],   "nilgph    - nil mismatch #{r['nilgph']}")
    assert_equal(@@nilvgph,   r['nilvgph'],  "nilvgph   - nil mismatch #{r['nilvgph']}")
    assert_equal(@@nilbin,    r['nilbin'],   "nilbin    - nil mismatch #{r['nilbin']}")
    assert_equal(@@nilvbin,   r['nilvbin'],  "nilvbin   - nil mismatch #{r['nilvbin']}")
    assert_equal(@@nildate,   r['nildate'],  "nildate   - nil mismatch #{r['nildate']}")
    assert_equal(@@niltime,   r['niltime'],  "niltime   - nil mismatch #{r['niltime']}")
    assert_equal(@@nilts,     r['nilts'],    "nilts     - nil mismatch #{r['nilts']}")
    assert_equal(@@nilclob,   r['nilclob'],  "nilclob   - nil mismatch #{r['nilclob']}")
    assert_equal(@@nildbclob, r['nildbclob'],"nildbclob - nil mismatch #{r['nildbclob']}")
    assert_equal(@@nilblob,   r['nilblob'],  "nilblob   - nil mismatch #{r['nilblob']}")
  end


  def test_0004_select_hex
    names = ["nilsmall",
             "nilint",
             "nilbig",
             "nildec",
             "nilnum",
             "nilflt",
             "nilreal",
             "nildbl",
             "nilchr",
             "nilvchr",
             "nilgph",
             "nilvgph",
             "nilbin",
             "nilvbin",
             "nildate",
             "niltime",
             "nilts",
             "nilclob",
             "nildbclob",
             "nilblob"]
    names.each { |n|
      stmt = IBM_DB::exec(@@conn,"select HEX(#{n}) as #{n}hex from NilMe")
      r = IBM_DB::fetch_assoc(stmt)
      puts "#{r}"
      stmt = IBM_DB::exec(@@conn,"select #{n} from NilMe")
      r = IBM_DB::fetch_assoc(stmt)
      puts "#{r}"
    }
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------

end
