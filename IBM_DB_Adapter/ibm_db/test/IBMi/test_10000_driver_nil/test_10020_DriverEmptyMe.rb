# File: test_10020_DriverPrepareTableEmptyMe.rb
require "../test_authorization/auth"
require "test/unit"

class Test_10020_DriverEmptyMe < Test::Unit::TestCase
  @@conn = nil
  @@eptsmall  = ''
  @@eptint    = ''
  @@eptbig    = ''
  @@eptdec    = ''
  @@eptnum    = ''
  @@eptflt    = ''
  @@eptreal   = ''
  @@eptdbl    = ''
  @@eptchr    = ''
  @@eptvchr   = ''
  @@eptgph    = ''
  @@eptvgph   = ''
  @@eptbin    = ''
  @@eptvbin   = ''
  @@eptdate   = ''
  @@epttime   = ''
  @@eptts     = ''
  @@eptclob   = ''
  @@eptdbclob = ''
  @@eptblob   = ''

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	  @@conn = ActiveRecord::Base.connection.connection
  end

  def test_0001_create_table
    begin
      stmt = IBM_DB::exec(@@conn,'DROP TABLE EmptyMe')
    rescue => msg
    end
    sql = "CREATE TABLE EmptyMe (" + 
          "eptsmall smallint," +
          "eptint integer," +
          "eptbig bigint," +
          "eptdec decimal(12,4)," +
          "eptnum numeric(12,4)," +
          "eptflt float," +
          "eptreal real," +
          "eptdbl double," +
          "eptchr char(12)," +
          "eptvchr varchar(12)," +
          "eptgph graphic(12)," +
          "eptvgph vargraphic(12)," +
          "eptbin binary(12)," +
          "eptvbin varbinary(12)," +
          "eptdate date," +
          "epttime time," +
          "eptts timestamp," +
          "eptclob clob(1M)," +
          "eptdbclob dbclob(1M)," +
          "eptblob blob(1M)" +
          ")"
    stmt = IBM_DB::exec(@@conn,sql)
  end

  def test_0002_insert_nil
    sql = "INSERT INTO EmptyMe (" +
          "eptsmall," +
          "eptint," +
          "eptbig," +
          "eptdec," +
          "eptnum," +
          "eptflt," +
          "eptreal," +
          "eptdbl," +
          "eptchr," +
          "eptvchr," +
          "eptgph," +
          "eptvgph," +
          "eptbin," +
          "eptvbin," +
          "eptdate," +
          "epttime," +
          "eptts," +
          "eptclob," +
          "eptdbclob," +
          "eptblob" +
          ")" +
           " VALUES (" +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?," +
          "?" +
          ")"
    stmt = IBM_DB::prepare(@@conn,sql)
    a = [
    @@eptsmall,
    @@eptint,
    @@eptbig,
    @@eptdec,
    @@eptnum,
    @@eptflt,
    @@eptreal,
    @@eptdbl,
    @@eptchr,
    @@eptvchr,
    @@eptgph,
    @@eptvgph,
    @@eptbin,
    @@eptvbin,
    @@eptdate,
    @@epttime,
    @@eptts,
    @@eptclob,
    @@eptdbclob,
    @@eptblob]
    ret = IBM_DB::execute(stmt,a)
  end

  def test_0003_select_nil
    stmt = IBM_DB::exec(@@conn,'select * from EmptyMe')
    puts IBM_DB::stmt_errormsg();
    r = IBM_DB::fetch_assoc(stmt)

    assert_equal("Fixnum",     r['eptsmall'].class.name, "eptbig    - mismatch")
    assert_equal("Fixnum",     r['eptint'].class.name,   "eptint    - mismatch")
    assert_equal("String",     r['eptbig'].class.name,   "eptbig    - mismatch")
    assert_equal("BigDecimal", r['eptdec'].class.name,   "eptdec    - mismatch")
    assert_equal("BigDecimal", r['eptnum'].class.name,   "eptnum    - mismatch")
    assert_equal("Float",      r['eptflt'].class.name,   "eptflt    - mismatch")
    assert_equal("Float",      r['eptreal'].class.name,  "eptreal   - mismatch")
    assert_equal("Float",      r['eptdbl'].class.name,   "eptdbl    - mismatch")
    assert_equal("String",     r['eptchr'].class.name,   "eptchr    - mismatch")
    assert_equal("String",     r['eptvchr'].class.name,  "eptvchr   - mismatch")
    assert_equal("String",     r['eptgph'].class.name,   "eptgph    - mismatch")
    assert_equal("String",     r['eptvgph'].class.name,  "eptvgph   - mismatch")
    assert_equal("String",     r['eptbin'].class.name,   "eptbin    - mismatch")
    assert_equal("String",     r['eptvbin'].class.name,  "eptvbin   - mismatch")
    assert_equal("NilClass",   r['eptdate'].class.name,  "eptdate   - mismatch")
    assert_equal("NilClass",   r['epttime'].class.name,  "epttime   - mismatch")
    assert_equal("NilClass",   r['eptts'].class.name,    "eptts     - mismatch")
    assert_equal("String",     r['eptclob'].class.name,  "eptclob   - mismatch")
    assert_equal("String",     r['eptdbclob'].class.name,"eptdbclob - mismatch")
    assert_equal("String",     r['eptblob'].class.name,  "eptblob   - mismatch")

    assert_equal(@@eptsmall,  r['eptsmall'],    "eptbig    - mismatch")
    assert_equal(@@eptint,    r['eptint'],      "eptint    - mismatch")
    assert_equal(@@eptbig,    r['eptbig'].to_i, "eptbig    - mismatch")
    assert_equal(@@eptdec,    r['eptdec'],      "eptdec    - mismatch")
    assert_equal(@@eptnum,    r['eptnum'],      "eptnum    - mismatch")
    assert_equal(@@eptflt,    r['eptflt'],      "eptflt    - mismatch")
    assert_equal(@@eptreal,   r['eptreal'],     "eptreal   - mismatch")
    assert_equal(@@eptdbl,    r['eptdbl'],      "eptdbl    - mismatch")
    assert_equal(@@eptchr,    r['eptchr'].strip,"eptchr    - mismatch")
    assert_equal(@@eptvchr,   r['eptvchr'],     "eptvchr   - mismatch")
    assert_equal(@@eptgph,    r['eptgph'].strip,"eptgph    - mismatch")
    assert_equal(@@eptvgph,   r['eptvgph'],     "eptvgph   - mismatch")
    assert_equal(@@eptbin,    r['eptbin'],      "eptbin    - mismatch")
    assert_equal(@@eptvbin,   r['eptvbin'],     "eptvbin   - mismatch")
    assert_equal(@@eptdate,   r['eptdate'],     "eptdate   - mismatch")
    assert_equal(@@epttime,   r['epttime'],     "epttime   - mismatch")
    assert_equal(@@eptts,     r['eptts'],       "eptts     - mismatch")
    assert_equal(@@eptclob,   r['eptclob'],     "eptclob   - mismatch")
    assert_equal(@@eptdbclob, r['eptdbclob'],   "eptdbclob - mismatch")
    assert_equal(@@eptblob,   r['eptblob'],     "eptblob   - mismatch")
  end

  def test_0004_select_hex
    names = ["eptsmall",
             "eptint",
             "eptbig",
             "eptdec",
             "eptnum",
             "eptflt",
             "eptreal",
             "eptdbl",
             "eptchr",
             "eptvchr",
             "eptgph",
             "eptvgph",
             "eptbin",
             "eptvbin",
             "eptdate",
             "epttime",
             "eptts",
             "eptclob",
             "eptdbclob",
             "eptblob"]
    names.each { |n|
      stmt = IBM_DB::exec(@@conn,"select HEX(#{n}) as #{n}hex from EmptyMe")
      r = IBM_DB::fetch_assoc(stmt)
      puts "#{r}"
      stmt = IBM_DB::exec(@@conn,"select #{n} from EmptyMe")
      r = IBM_DB::fetch_assoc(stmt)
      puts "#{r}"
    }
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------
  def wait_here
    for i in 0..2 
      puts "waiting #{Process.pid}"
      sleep(10)
    end
  end

end
