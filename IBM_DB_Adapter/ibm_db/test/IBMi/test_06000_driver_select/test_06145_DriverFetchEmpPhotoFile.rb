# File: test_06145_DriverFetchEmpPhotoFile.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_06145_DriverFetchEmpPhotoFile < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0011_select_fetch_array
    idx = 0
    stmt = IBM_DB::exec(@@conn,'select * from EMP_PHOTO_FILE')
    while row = IBM_DB::fetch_array(stmt)
      assert_match( $employee_photos[idx][0], row[0], "EMPNO mismatch")
      assert_match( $employee_photos[idx][1], row[1], "PHOTO_FORMAT mismatch")
      picture = File.dirname(File.realpath(__FILE__)).gsub("test_06000_driver_select","") + "test_artifacts/#{$employee_photos[idx][2]}"
      puts "\n #{picture}"
      size = File.size(picture)
      data = IO.binread(picture,size)
      hexdata = data.unpack("H*")
      hexlob = row[2].unpack("H*")
      assert_equal(hexdata, hexlob, "#{idx} PICTURE mismatch")
      idx += 1
    end
  end

end

