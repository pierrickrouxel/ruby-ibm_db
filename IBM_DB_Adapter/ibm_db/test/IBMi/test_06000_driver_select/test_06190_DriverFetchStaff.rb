# File: test_06190_DriverFetchStaff.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_06190_DriverFetchStaff < Test::Unit::TestCase
  @@conn = nil
  @@staff = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
    @@staff = Array.new
    for i in 0..9
     @@staff[i] = $staffs[i]
    end
  end

  def test_0011_select_fetch_array
    idx = 0
    stmt = IBM_DB::exec(@@conn,'select * from STAFF')
    while row = IBM_DB::fetch_array(stmt)
      puts "exp #{idx} #{$staffs[idx][0]} #{$staffs[idx][1]} #{$staffs[idx][2]} #{$staffs[idx][3]}  #{$staffs[idx][4]}  #{$staffs[idx][5]}  #{$staffs[idx][6]} "
      puts "row #{idx} #{row[0]} #{row[1]} #{row[2]} #{row[3]}  #{row[4]}  #{row[5]}  #{row[6]} "
      assert_equal( $staffs[idx][0], row[0], "ID mismatch")
      assert_match( $staffs[idx][1], row[1], "NAME mismatch")
      assert_equal( $staffs[idx][2], row[2], "DEPT mismatch")
      assert_match( $staffs[idx][3], row[3], "JOB mismatch")
      assert_equal( $staffs[idx][4], row[4], "YEARS mismatch")
      assert_equal( $staffs[idx][5], row[5], "SALARY mismatch")
      if row[6] != nil
        assert_match( $staffs[idx][6].to_s, row[6].to_s, "COMM mismatch")
      else
        assert_equal( $staffs[idx][6], row[6], "COMM mismatch")
      end
      idx += 1
    end
    count = $staffs.count
    assert_equal(count, idx, "missing row data")
  end

  def test_0401_fetch_staff_id_less_101
    idx = 0
    stmt = IBM_DB::exec(@@conn,'SELECT * FROM staff WHERE id < 101')
    while row = IBM_DB::fetch_array(stmt)
      puts "exp #{idx} #{@@staff[idx][0]} #{@@staff[idx][1]} #{@@staff[idx][2]} #{@@staff[idx][3]}  #{@@staff[idx][4]}  #{@@staff[idx][5]}  #{@@staff[idx][6]} "
      puts "row #{idx} #{row[0]} #{row[1]} #{row[2]} #{row[3]}  #{row[4]}  #{row[5]}  #{row[6]} "
      assert_equal( @@staff[idx][0], row[0], "ID mismatch")
      assert_match( @@staff[idx][1], row[1], "NAME mismatch")
      assert_equal( @@staff[idx][2], row[2], "DEPT mismatch")
      assert_match( @@staff[idx][3], row[3], "JOB mismatch")
      assert_equal( @@staff[idx][4], row[4], "YEARS mismatch")
      assert_equal( @@staff[idx][5], row[5], "SALARY mismatch")
      if row[6] != nil
        assert_match( @@staff[idx][6].to_s, row[6].to_s, "COMM mismatch")
      else
        assert_equal( @@staff[idx][6], row[6], "COMM mismatch")
      end
      idx += 1
    end
    count = 10
    assert_equal(count, idx, "missing row data")
  end

  def test_0402_fetch_staff_id_less_101_nested
    nest = 0
    stmt1 = IBM_DB::exec(@@conn,'SELECT * FROM staff WHERE id < 101')
    while row = IBM_DB::fetch_array(stmt1)
     idx = 0
     stmt2 = IBM_DB::exec(@@conn,'SELECT * FROM staff WHERE id < 101')
     while row = IBM_DB::fetch_array(stmt2)
      if nest == idx 
        puts "nest #{nest} exp #{idx} #{@@staff[idx][0]} #{@@staff[idx][1]} #{@@staff[idx][2]} #{@@staff[idx][3]}  #{@@staff[idx][4]}  #{@@staff[idx][5]}  #{@@staff[idx][6]} "
        puts "nest #{nest} row #{idx} #{row[0]} #{row[1]} #{row[2]} #{row[3]}  #{row[4]}  #{row[5]}  #{row[6]} "
      end
      assert_equal( @@staff[idx][0], row[0], "ID mismatch")
      assert_match( @@staff[idx][1], row[1], "NAME mismatch")
      assert_equal( @@staff[idx][2], row[2], "DEPT mismatch")
      assert_match( @@staff[idx][3], row[3], "JOB mismatch")
      assert_equal( @@staff[idx][4], row[4], "YEARS mismatch")
      assert_equal( @@staff[idx][5], row[5], "SALARY mismatch")
      if row[6] != nil
        assert_match( @@staff[idx][6].to_s, row[6].to_s, "COMM mismatch")
      else
        assert_equal( @@staff[idx][6], row[6], "COMM mismatch")
      end
      idx += 1
     end
     nest += 1
    end
    count = 100
    assert_equal(count, idx * nest, "missing row data")
  end

end

