# File: test_06150_DriverFetchOrg.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_06150_DriverFetchOrg < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0011_select_fetch_array
    idx = 0
    stmt = IBM_DB::exec(@@conn,'select * from ORG')
    while row = IBM_DB::fetch_array(stmt)
      puts "exp #{idx} #{$orgs[idx][0]} #{$orgs[idx][1]} #{$orgs[idx][2]} #{$orgs[idx][3]} #{$orgs[idx][4]}"
      puts "row #{idx} #{row[0]} #{row[1]} #{row[2]} #{row[3]} #{row[4]}"
      assert_equal( $orgs[idx][0], row[0], "DEPTNUMB mismatch")
      assert_match( $orgs[idx][1], row[1], "DEPTNAME mismatch")
      assert_equal( $orgs[idx][2], row[2], "MANAGER mismatch")
      assert_match( $orgs[idx][3], row[3], "DIVISION mismatch")
      assert_match( $orgs[idx][4], row[4], "LOCATION mismatch")
      idx += 1
    end
    count = $orgs.count
    assert_equal(count, idx, "missing row data")
  end

end

