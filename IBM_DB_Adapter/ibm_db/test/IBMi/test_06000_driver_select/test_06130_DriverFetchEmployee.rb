# File: test_06130_DriverFetchEmployee.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_06130_DriverFetchEmployee < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0011_select_fetch_array
    idx = 0
    stmt = IBM_DB::exec(@@conn,'select * from EMPLOYEE')
    while row = IBM_DB::fetch_array(stmt)
      puts "exp #{idx} #{$employees[idx][0]} #{$employees[idx][1]} #{$employees[idx][2]} #{$employees[idx][3]} #{$employees[idx][4]} #{$employees[idx][5]} #{$employees[idx][6]} #{$employees[idx][7]} #{$employees[idx][8]} #{$employees[idx][9]} #{$employees[idx][10]} #{$employees[idx][11]} #{$employees[idx][12]} #{$employees[idx][13]}"
      puts "row #{idx} #{row[0]} #{row[1]} #{row[2]} #{row[3]} #{row[4]} #{row[5]} #{row[6]} #{row[7]} #{row[8]} #{row[9]} #{row[10]} #{row[11]} #{row[12]} #{row[13]}"
      assert_match( $employees[idx][0], row[0], "EMPNO mismatch")
      assert_match( $employees[idx][1], row[1], "FIRSTNME mismatch")
      assert_match( $employees[idx][2], row[2], "MIDINIT mismatch")
      assert_match( $employees[idx][3], row[3], "LASTNAME mismatch")
      assert_match( $employees[idx][4], row[4], "WORKDEPT mismatch")
      assert_match( $employees[idx][5], row[5], "PHONENO mismatch")
      assert_match( $employees[idx][6], row[6], "HIREDATE mismatch")
      assert_match( $employees[idx][7], row[7], "JOB mismatch")
      assert_equal( $employees[idx][8], row[8], "EDLEVEL mismatch nil")
      assert_match( $employees[idx][9], row[9], "SEX mismatch")
      assert_match( $employees[idx][10], row[10], "BIRTHDATE mismatch")
      assert_equal( $employees[idx][11], row[11], "SALARY mismatch")
      assert_equal( $employees[idx][12], row[12], "BONUS mismatch")
      assert_equal( $employees[idx][13], row[13], "COMM mismatch")
      idx += 1
    end
    count = $employees.count
    assert_equal(count, idx, "missing row data")
  end

end


