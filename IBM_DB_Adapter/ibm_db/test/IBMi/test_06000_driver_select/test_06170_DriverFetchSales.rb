# File: test_06170_DriverFetchSales.rb
require "../test_authorization/auth"
require "../test_data/company"
require "test/unit"

class Test_06170_DriverFetchSales < Test::Unit::TestCase
  @@conn = nil

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------

  def test_0000_connect
	@@conn = ActiveRecord::Base.connection.connection
  end

  def test_0011_select_fetch_array
    idx = 0
    stmt = IBM_DB::exec(@@conn,'select * from SALES')
    while row = IBM_DB::fetch_array(stmt)
      puts "exp #{idx} #{$sales[idx][0]} #{$sales[idx][1]} #{$sales[idx][2]} ( #{$sales[idx][3]} )"
      puts "row #{idx} #{row[0]} #{row[1]} #{row[2]} ( #{row[3]} )"
      assert_match( $sales[idx][0], row[0], "SALES_DATE mismatch")
      assert_match( $sales[idx][1], row[1], "SALES_PERSON mismatch")
      assert_match( $sales[idx][2], row[2], "REGION mismatch")
      assert_equal( $sales[idx][3], row[3], "SALES mismatch nil")
      idx += 1
    end
    count = $sales.count
    assert_equal(count, idx, "missing row data")
  end

end

