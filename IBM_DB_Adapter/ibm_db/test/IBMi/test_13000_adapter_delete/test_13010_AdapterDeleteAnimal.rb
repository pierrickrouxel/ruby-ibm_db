# File: test_13010_AdapterDeleteAnimal.rb
# ActiveRecord::Base.establish_connection keywords (see auth_active)
require "../test_authorization/auth_active"
require "../test_data/animal"
require "../test_data/rowcol"
require 'rubygems'
require 'active_record'

class AnimalActiveRecord < ActiveRecord::Base
  self.table_name = "ANIMALS"
end

class Test_13010_AdapterDeleteAnimal < RowColUnitTest
  @@col_all = [0,1,2,3]
  @@row_all = [0,1,2,3,4,5,6]
  @@delete_even = [0,2,4,6]
  @@row_after_delete_even = [1,3,5]

  # -------------------------------
  # test functions (start with prefix test_)
  # rake test -- Unit run alpha sort order
  # run #1) test_0000_connect
  # run #2) test_nnnn_xxx
  # :
  # run #n) test_9999_close
  # -------------------------------
  def setup()
    self.init($animal_col, $animals, @@row_all, @@col_all)
    @@auth.verbose_global()
  end

  def test_0010_delete_all
    @@auth.verbose "begin transaction"
    AnimalActiveRecord.transaction do
      AnimalActiveRecord.delete_all
      animals = AnimalActiveRecord.find(:all, :order => 'ID')
      nbr = animals.count
      @@auth.verbose "delete all count = #{nbr}"
      assert_equal( 0, animals.count, "delete all count mismatch")
      @@auth.verbose "rollback now"
      raise ActiveRecord::Rollback
    end
    @@auth.verbose "end transaction"
    self.animal_check_rollback(__method__)
  end


  def test_0010_delete_even
    @@auth.verbose "begin transaction"
    AnimalActiveRecord.transaction do
      AnimalActiveRecord.delete(@@delete_even)
      animals = AnimalActiveRecord.find(:all, :order => 'ID')
      all = Array.new
      animals.each { |row|
        all << [ 
               row.id, 
               row.breed, 
               row.name, 
               row.weight 
               ]
      }
      self.animal_check_array(__method__,all,@@row_after_delete_even)
      @@auth.verbose "rollback now"
      raise ActiveRecord::Rollback
    end
    @@auth.verbose "end transaction"
    self.animal_check_rollback(__method__)
  end

  # -------------------------------
  # non-test functions (no prefix test_)
  # -------------------------------
  def animal_check_rollback(test)
    animals = AnimalActiveRecord.find(:all, :order => 'ID')
    all = Array.new
    animals.each { |row|
      all << [ 
             row.id, 
             row.breed, 
             row.name, 
             row.weight 
             ]
    }
    self.animal_check_array(test,all)
  end

  def animal_check_array(test, all, row_exp = @@row_all , col_exp = @@col_all)
    self.rowcol_check_array(test, all, row_exp, col_exp)
  end

end
