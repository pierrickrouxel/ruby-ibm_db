#!/bin/bash

#Create area to install gem outside of /PowerRuby
mkdir -p /ruby/gemsets/ruby-ibm_db
export GEM_HOME=/ruby/gemsets/ruby-ibm_db
export GEM_PATH=/ruby/gemsets/ruby-ibm_db:/PowerRuby/prV2R1/lib/ruby/gems/2.1.0
echo "GEM_HOME:$GEM_HOME"
echo "GEM_PATH:$GEM_PATH"

GEM_NAME="ibm_db-2.5.14"
rm $GEM_NAME.gem
rm -R $GEM_NAME.gem.build
echo 'build'
gem build IBM_DB.gemspec
echo "install $GEM_NAME.gem"
gem install ./$GEM_NAME.gem --local --force --ignore-dependencies --no-ri --no-rdoc
if ! (gem list gem-compiler -i); then
  echo "Installing Gem gem-compiler"
  gem install gem-compiler
fi
echo "Compiling 'fat binary' gem"
gem compile $GEM_NAME.gem
# LIBPATH=/PowerRuby/prV2R1/lib:/home/ADC/PaseCliTrace
case "${LIBPATH}" in
  "")
    # echo "empty"
  ;;
  *prV2R0*)
    echo "copy prV2R0 $GEM_NAME to $GEM_NAME-powerpc-aix-6"
    cp -R /PowerRuby/prV2R0/lib/ruby/gems/2.0.0/gems/$GEM_NAME/*  /PowerRuby/prV2R0/lib/ruby/gems/2.0.0/gems/$GEM_NAME-powerpc-aix-6/.
  ;;
  *prV2R1*)
    echo "copy prV2R1 $GEM_NAME to $GEM_NAME-powerpc-aix-6"
    cp -R /PowerRuby/prV2R1/lib/ruby/gems/2.1.0/gems/$GEM_NAME/*  /PowerRuby/prV2R1/lib/ruby/gems/2.1.0/gems/$GEM_NAME-powerpc-aix-6/.
  ;;
  *)
    echo "no copy $GEM_NAME to $GEM_NAME-powerpc-aix-6 (LIBPATH=${LIBPATH})"
  ;;
esac

echo 'done'

