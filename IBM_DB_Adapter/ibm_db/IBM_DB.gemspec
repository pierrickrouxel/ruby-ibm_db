# +----------------------------------------------------------------------+
# |  Licensed Materials - Property of IBM                                |
# |                                                                      |
# | (C) Copyright IBM Corporation 2006, 2007,2008,2009,2010              |
# +----------------------------------------------------------------------+

require 'rubygems'
require 'pathname'

SPEC = Gem::Specification.new do |gem|
  gem.name    = 'ibm_db'
  gem.version = '2.5.14'

  gem.summary = "Rails Driver and Adapter"
  gem.description = "IBM Data Servers: {DB2 on i5/OS}"

  gem.authors  = ['IBM']
  gem.license = 'MIT'
  gem.email    = 'adc@us.ibm.com'
  gem.homepage = 'https://github.com/rangercairns/ruby-ibmdb'

  gem.add_dependency('rake')
  gem.add_dependency('activesupport')
  gem.add_dependency('activemodel')
  gem.add_dependency('arel')

  gem.extensions = 'ext/extconf.rb'
  
  gem.files =  Dir.glob("ext/**/*.{h,c,rb}") + 
               Dir['lib/**/*', 'README*', 'LICENSE*', 'README*', 'CHANGES*', 'MANIFEST*', 'version*']
end

if $0 == __FILE__
  Gem::manage_gems
  Gem::Builder.new(spec).build
end

return SPEC
