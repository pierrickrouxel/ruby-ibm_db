#!/usr/bin/env ruby

# +----------------------------------------------------------------------+
# |  Licensed Materials - Property of IBM                                |
# |                                                                      |
# | (C) Copyright IBM Corporation 2006 - 2012                            |
# +----------------------------------------------------------------------+

WIN = RUBY_PLATFORM =~ /mswin/ || RUBY_PLATFORM =~ /mingw/

# check PASE uname due to RUBY_PLATFORM ambiguous powerpc_aix-n.n.n
if (WIN.nil?)
  DO_PASE = `uname` =~ /400/
end
# user supplied IBM_DB_UTF8 for DO_UTF8_ONLY compile
DO_UTF8_ONLY = ENV['IBM_DB_UTF8']
if (!DO_PASE.nil? || !DO_UTF8_ONLY.nil?)
  DO_UTF8_VERSION = "yes"
else
  DO_UTF8_VERSION = nil
end
# 1.8 retired July, 2013
if( !(RUBY_VERSION =~ /1.8/) || !DO_UTF8_VERSION.nil?)
  DO_UNICODE_VERSION = "yes"
end

# use ENV['IBM_DB_HOME'] or latest db2 you can find
IBM_DB_HOME = ENV['IBM_DB_HOME']

machine_bits = ['ibm'].pack('p').size * 8

is64Bit = true

module Kernel
  def suppress_warnings
    origVerbosity = $VERBOSE
    $VERBOSE = nil
    result = yield
    $VERBOSE = origVerbosity
    return result
  end
end

# speak to me
if machine_bits == 64
  is64Bit = true
  puts "Detected 64-bit Ruby\n "
else
  is64Bit = false
  puts "Detected 32-bit Ruby\n "
end
if(DO_UNICODE_VERSION.nil?)
  puts "compile type 1) Ruby <  1.9\n"
else
  if (DO_PASE.nil? && DO_UTF8_ONLY.nil?)
    puts "compile type 2) Ruby >= 1.9 UNICODE_SUPPORT_VERSION"
  else
    if (!DO_PASE.nil?)
      puts "compile type 3) Ruby >= 1.9 IBM i PASE_RELEASE_VERSION"
    end
    if (!DO_UTF8_ONLY.nil?)
      puts "compile type 4) Ruby  >= 1.9 LUW UTF8_ONLY UTF8_OVERRIDE_VERSION"
    end
  end
end

# ============================
# LUW, other, UTF8_ONLY path
# ============================
if (DO_PASE.nil?)

 if(IBM_DB_HOME == nil || IBM_DB_HOME == '')
  IBM_DB_INCLUDE = ENV['IBM_DB_INCLUDE']
  IBM_DB_LIB = ENV['IBM_DB_LIB']
  
  if( ( (IBM_DB_INCLUDE.nil?) || (IBM_DB_LIB.nil?) ) ||
      ( IBM_DB_INCLUDE == '' || IBM_DB_LIB == '' )
	)
    puts "Environment variable IBM_DB_HOME is not set. Set it to your DB2/IBM_Data_Server_Driver installation directory and retry gem install.\n "
    exit 1
  end
 else
  IBM_DB_INCLUDE = "#{IBM_DB_HOME}/include"
  
  if(is64Bit)
    IBM_DB_LIB="#{IBM_DB_HOME}/lib64"
  else
    IBM_DB_LIB="#{IBM_DB_HOME}/lib32"
  end
 end

 if( !(File.directory?(IBM_DB_LIB)) )
  suppress_warnings{IBM_DB_LIB = "#{IBM_DB_HOME}/lib"}
  if( !(File.directory?(IBM_DB_LIB)) )
    puts "Cannot find #{IBM_DB_LIB} directory. Check if you have set the IBM_DB_HOME environment variable's value correctly\n "
	exit 1
  end
  notifyString  = "Detected usage of IBM Data Server Driver package. Ensure you have downloaded "
  
  if(is64Bit)
    notifyString = notifyString + "64-bit package "
  else
    notifyString = notifyString + "32-bit package "
  end
  notifyString = notifyString + "of IBM_Data_Server_Driver and retry the 'gem install ibm_db' command\n "
  
  puts notifyString
 end

 if( !(File.directory?(IBM_DB_INCLUDE)) )
  puts " #{IBM_DB_HOME}/include folder not found. Check if you have set the IBM_DB_HOME environment variable's value correctly\n "
  exit 1
 end

end

require 'mkmf'

# ============================
# PASE only path
# ============================

if (!DO_PASE.nil?)
 if ($CFLAGS.nil?)
  $CFLAGS = ' '
 end
 $CFLAGS << ' ' + '-DPASE'

 if ($LIBS.nil?)
  $LIBS = ' '
 end
 $LIBS << ' ' + '-liconv -ldb400'

 IBM_DB_HOME = "/usr"
 IBM_DB_INCLUDE = "#{IBM_DB_HOME}/include"
 IBM_DB_LIB="#{IBM_DB_HOME}/lib"
end # DO_PASE only path

# UTF8_ONLY
if (!DO_UTF8_VERSION.nil?)
 if ($CFLAGS.nil?)
  $CFLAGS = ' '
 end
 $CFLAGS << ' ' + '-DUTF8_ONLY'
end

# make your compile choice understood
if(!DO_UNICODE_VERSION.nil?)
  create_header('gil_release_version')
  create_header('unicode_support_version')
end

dir_config('IBM_DB',IBM_DB_INCLUDE,IBM_DB_LIB)

def crash(str)
  printf(" extconf failure: %s\n", str)
  exit 1
end

# ============================
# LUW, other, UTF8_ONLY path
# ============================
if (DO_PASE.nil?)

unless (have_library(WIN ? 'db2cli' : 'db2','SQLConnect') or find_library(WIN ? 'db2cli' : 'db2','SQLConnect', IBM_DB_LIB))
  crash(<<EOL)
Unable to locate libdb2.so/a under #{IBM_DB_LIB}

Follow the steps below and retry

Step 1: - Install IBM DB2 Universal Database Server/Client

step 2: - Set the environment variable IBM_DB_HOME as below
        
             (assuming bash shell)
        
             export IBM_DB_HOME=<DB2/IBM_Data_Server_Driver installation directory> #(Eg: export IBM_DB_HOME=/opt/ibm/db2/v10)

step 3: - Retry gem install
        
EOL
end

alias :libpathflag0 :libpathflag
 def libpathflag(libpath)
  libpathflag0 + case Config::CONFIG["arch"]
    when /solaris2/
      libpath[0..-2].map {|path| " -R#{path}"}.join
    when /linux/
      libpath[0..-2].map {|path| " -Wl,-rpath,#{path}"}.join
    else
      ""
  end
end

end

have_header('gil_release_version')
have_header('unicode_support_version')

create_makefile('ibm_db')

