/*
  +----------------------------------------------------------------------+
  |  Licensed Materials - Property of IBM                                |
  |                                                                      |
  | (C) Copyright IBM Corporation 2006, 2007, 2008, 2009, 2010, 2012     |
  +----------------------------------------------------------------------+
  | Authors: Sushant Koduru, Lynh Nguyen, Kanchana Padmanabhan,          |
  |          Dan Scott, Helmut Tessarek, Kellen Bombardier, Sam Ruby     |
  |          Ambrish Bhargava, Tarun Pasrija, Praveen Devarao            |
  |          Tony Cairns                                                 |
  +----------------------------------------------------------------------+
*/
/* ==============================
 * Story of sql_luw.h and sql_pase.h ... 
 *   see sql_com.h to understand.
 * ============================== 
 */

#ifndef RUBY_SQL_PASE_H
#define RUBY_SQL_PASE_H

#ifdef PASE
/* IBM i missing completely */
typedef long long SQLBIGINT;                                          
#define SQLLEN SQLINTEGER                                          
#define SQLULEN SQLUINTEGER
#define SQL_IS_INTEGER 0
#define SQL_BEST_ROWID 0
#define SQLFLOAT SQLREAL
#define SQL_C_SBIGINT SQL_C_BIGINT
/* IBM i signed CLI APIs (LUW unsigned APIs) */                                      
#define SQLUSMALLINT SQLSMALLINT                                      
#define SQLUINTEGER SQLINTEGER

#define SQL_IS_INTEGER 0
#define SQL_IS_UINTEGER 0 
#define SQL_BEST_ROWID 0
#define SQLLEN long
#define SQLFLOAT double
#define SQLUINTEGER SQLINTEGER
#define SQLUSMALLINT SQLSMALLINT
/*** incompatible V6 change IBM i */
#define  SQL_BINARY_V6 -2
#define  SQL_VARBINARY_V6 -3
#define  SQL_C_BINARY_V6 SQL_BINARY_V6
/* cross compile V6+ to V5 */
#undef SQL_BINARY
#undef SQL_C_BINARY
#undef SQL_VARBINARY
#define SQL_BINARY 97
#define SQL_C_BINARY SQL_BINARY
#define SQL_VARBINARY 98
/* long same ordinal (mess up case statements) */
#undef SQL_LONGVARCHAR
#define SQL_LONGVARCHAR SQL_TYPE_MISSING2 
#undef SQL_LONGVARGRAPHIC
#define SQL_LONGVARGRAPHIC SQL_TYPE_MISSING3
#undef SQL_LONGVARBINARY
#define SQL_LONGVARBINARY SQL_TYPE_MISSING4
#undef SQL_WLONGVARCHAR
#define SQL_WLONGVARCHAR SQL_TYPE_MISSING5

#ifndef SQL_SCROLLABLE /* maybe missing IBM i */
#define SQL_SCROLLABLE SQL_CURSOR_DYNAMIC
#endif
#ifndef SQL_SCROLL_FORWARD_ONLY /* maybe missing IBM i */
#define SQL_SCROLL_FORWARD_ONLY SQL_CURSOR_FORWARD_ONLY
#endif
#undef SQL_CURSOR_KEYSET_DRIVEN
#define SQL_CURSOR_KEYSET_DRIVEN SQL_CURSOR_STATIC

#ifndef SQL_ATTR_QUERY_TIMEOUT /* maybe missing IBM i */
#define SQL_ATTR_QUERY_TIMEOUT SQL_QUERY_TIMEOUT
#endif

#ifndef SQL_ATTR_PING_DB /* maybe missing IBM i */
#define SQL_ATTR_PING_DB SQL_ATTR_MISSING
#endif

#ifndef SQL_ATTR_ODBC_VERSION /* maybe missing IBM i */
#define SQL_ATTR_ODBC_VERSION SQL_ATTR_MISSING
#endif
#ifndef SQL_OV_ODBC3 /* maybe missing IBM i */
#define SQL_OV_ODBC3 SQL_ATTR_MISSING
#endif

#ifndef SQL_QUICK /* maybe missing IBM i */
#define SQL_QUICK 0
#endif

#ifndef SQL_DATABASE_CODEPAGE /* maybe missing IBM i */
#define SQL_DATABASE_CODEPAGE SQL_ATTR_MISSING
#endif
#ifndef SQL_SERVER_NAME /* maybe missing IBM i */
#define SQL_SERVER_NAME SQL_ATTR_MISSING
#endif
#ifndef SQL_SPECIAL_CHARACTERS /* maybe missing IBM i */
#define SQL_SPECIAL_CHARACTERS SQL_ATTR_MISSING
#endif
#ifndef SQL_MAX_IDENTIFIER_LEN /* maybe missing IBM i */
#define SQL_MAX_IDENTIFIER_LEN SQL_ATTR_MISSING
#endif
#ifndef SQL_MAX_INDEX_SIZE /* maybe missing IBM i */
#define SQL_MAX_INDEX_SIZE SQL_ATTR_MISSING
#endif
#ifndef SQL_MAX_PROCEDURE_NAME_LEN /* maybe missing IBM i */
#define SQL_MAX_PROCEDURE_NAME_LEN SQL_ATTR_MISSING
#endif
#ifndef SQL_ODBC_VER /* maybe missing IBM i */
#define SQL_ODBC_VER SQL_ATTR_MISSING
#endif
#ifndef SQL_APPLICATION_CODEPAGE /* maybe missing IBM i */
#define SQL_APPLICATION_CODEPAGE SQL_ATTR_MISSING
#endif
#ifndef SQL_CONNECT_CODEPAGE /* maybe missing IBM i */
#define SQL_CONNECT_CODEPAGE SQL_ATTR_MISSING
#endif
#ifndef SQL_IS_POINTER /* maybe missing IBM i */
#define SQL_IS_POINTER SQL_ATTR_MISSING
#endif
#ifndef SQL_ATTR_CHAINING_BEGIN /* maybe missing IBM i */
#define SQL_ATTR_CHAINING_BEGIN SQL_ATTR_MISSING
#endif
#ifndef SQL_ATTR_CHAINING_END /* maybe missing IBM i */
#define SQL_ATTR_CHAINING_END SQL_ATTR_MISSING
#endif


#ifndef SQL_ATTR_CONN_SORT_SEQUENCE
#define SQL_ATTR_CONN_SORT_SEQUENCE  10046
#endif
#ifndef SQL_HEX_SORT_SEQUENCE
#define SQL_HEX_SORT_SEQUENCE 0                   
#endif
#ifndef SQL_JOB_SORT_SEQUENCE
#define SQL_JOB_SORT_SEQUENCE 1                   
#endif
#ifndef SQL_JOBRUN_SORT_SEQUENCE
#define SQL_JOBRUN_SORT_SEQUENCE 2                   
#endif

#ifndef SQL_FETCH_ABSOLUTE /* maybe missing IBM i */
#define SQL_FETCH_ABSOLUTE 5
#endif

#ifndef SQL_TXN_ISOLATION_OPTION
#define SQL_TXN_ISOLATION_OPTION SQL_DEFAULT_TXN_ISOLATION
#endif

#ifndef SQL_CONCUR_DEFAULT
#define SQL_CONCUR_DEFAULT SQL_CONCUR_READ_ONLY /* Default value */
#endif

#ifndef SQL_ATTR_DEFERRED_PREPARE
#define SQL_ATTR_DEFERRED_PREPARE SQL_ATTR_MISSING
#define SQL_DEFERRED_PREPARE_ON SQL_TYPE_MISSING2
#define SQL_DEFERRED_PREPARE_OFF SQL_TYPE_MISSING3
#endif

/* allow PASE to use IBMi_ attrs */
#define SQL_IBMi_FMT_ISO SQL_FMT_ISO
#define SQL_IBMi_FMT_USA SQL_FMT_USA
#define SQL_IBMi_FMT_EUR SQL_FMT_EUR
#define SQL_IBMi_FMT_JIS SQL_FMT_JIS
#define SQL_IBMi_FMT_MDY SQL_FMT_DMY
#define SQL_IBMi_FMT_DMY SQL_FMT_MDY
#define SQL_IBMi_FMT_YMD SQL_FMT_YMD
#define SQL_IBMi_FMT_JUL SQL_FMT_JUL
#define SQL_IBMi_FMT_JOB SQL_FMT_JOB
#define SQL_IBMi_FMT_HMS SQL_FMT_HMS

#ifndef SQL_ATTR_ROWCOUNT_PREFETCH
#define SQL_ATTR_ROWCOUNT_PREFETCH SQL_ATTR_MISSING
#define SQL_ROWCOUNT_PREFETCH_OFF SQL_TYPE_MISSING2
#define SQL_ROWCOUNT_PREFETCH_ON SQL_TYPE_MISSING3
#endif

#ifndef SQL_ATTR_INFO_USERID
#define SQL_ATTR_INFO_USERID 10103
#define SQL_ATTR_INFO_WRKSTNNAME 10104
#define SQL_ATTR_INFO_APPLNAME 10105
#define SQL_ATTR_INFO_ACCTSTR 10106
#define SQL_ATTR_INFO_PROGRAMID 10107
#endif

#ifndef SQL_ATTR_USE_TRUSTED_CONTEXT
#define SQL_ATTR_USE_TRUSTED_CONTEXT SQL_ATTR_MISSING
#define SQL_ATTR_TRUSTED_CONTEXT_USERID SQL_TYPE_MISSING2
#define SQL_ATTR_TRUSTED_CONTEXT_PASSWORD SQL_TYPE_MISSING3
#endif

#ifndef SQL_ATTR_GET_GENERATED_VALUE
#define SQL_ATTR_GET_GENERATED_VALUE SQL_ATTR_MISSING
#endif

#endif /* PASE */
#endif /* RUBY_SQL_PASE_H */

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 */

