/*
  +----------------------------------------------------------------------+
  |  Licensed Materials - Property of IBM                                |
  |                                                                      |
  | (C) Copyright IBM Corporation 2006, 2007, 2008, 2009, 2010, 2012     |
  +----------------------------------------------------------------------+
  | Authors: Sushant Koduru, Lynh Nguyen, Kanchana Padmanabhan,          |
  |          Dan Scott, Helmut Tessarek, Kellen Bombardier, Sam Ruby     |
  |          Ambrish Bhargava, Tarun Pasrija, Praveen Devarao            |
  |          Tony Cairns                                                 |
  +----------------------------------------------------------------------+
*/
/* ==============================
 * Story of sql_luw.h and sql_pase.h ... 
 *   see sql_com.h to understand.
 * ============================== 
 */
#ifndef RUBY_SQL_LUW_H
#define RUBY_SQL_LUW_H

#ifndef PASE

/* Needed for Backward compatibility */
#ifndef SQL_XML
#define SQL_XML -370
#endif

/* Needed for Backward compatibility */
#ifndef SQL_DECFLOAT
#define SQL_DECFLOAT -360
#endif

#define SQL_UTF8_CHAR SQL_TYPE_MISSING2
#define SQL_VARBINARY_V6 SQL_TYPE_MISSING3
#define SQL_BINARY_V6 SQL_TYPE_MISSING4
#define SQL_C_BINARY_V6 SQL_BINARY_V6

#ifndef SQL_ATTR_SERVER_MODE /* maybe missing LUW */
#define SQL_ATTR_SERVER_MODE SQL_ATTR_MISSING
#endif

#ifndef SQL_ATTR_INCLUDE_NULL_IN_LEN /* maybe missing LUW */
#define SQL_ATTR_INCLUDE_NULL_IN_LEN SQL_ATTR_MISSING
#endif

#ifndef SQL_ATTR_DBC_SYS_NAMING /* maybe missing LUW */
#define SQL_ATTR_DBC_SYS_NAMING 3017
#endif
#ifndef SQL_HEX_SORT_SEQUENCE
#define SQL_HEX_SORT_SEQUENCE SQL_ATTR_MISSING                   
#endif
#ifndef SQL_JOB_SORT_SEQUENCE
#define SQL_JOB_SORT_SEQUENCE SQL_ATTR_MISSING                   
#endif
#ifndef SQL_JOBRUN_SORT_SEQUENCE
#define SQL_JOBRUN_SORT_SEQUENCE SQL_ATTR_MISSING                   
#endif

#ifndef SQL_ATTR_CONN_SORT_SEQUENCE /* maybe missing LUW */
#define SQL_ATTR_CONN_SORT_SEQUENCE SQL_ATTR_MISSING
#endif


/* connect isolation */
#ifndef SQL_TXN_NO_COMMIT /* LUW missing alias */
#define SQL_TXN_NO_COMMIT SQL_TXN_NOCOMMIT
#endif

#ifndef SQL_ATTR_DBC_DEFAULT_LIB
#define SQL_ATTR_DBC_DEFAULT_LIB SQL_ATTR_MISSING
#endif

/* connect date time format */
#ifndef SQL_ATTR_DATE_FMT /* maybe missing LUW */
#define SQL_ATTR_DATE_FMT 3025
#endif
#ifndef SQL_ATTR_TIME_FMT /* maybe missing LUW */
#define SQL_ATTR_TIME_FMT 3027 
#endif

#ifndef SQL_IBMi_FMT_ISO /* maybe missing LUW */
#define SQL_IBMi_FMT_ISO 1
#endif
#ifndef SQL_IBMi_FMT_USA /* maybe missing LUW */
#define SQL_IBMi_FMT_USA 2
#endif
#ifndef SQL_IBMi_FMT_EUR /* maybe missing LUW */
#define SQL_IBMi_FMT_EUR 3
#endif
#ifndef SQL_IBMi_FMT_JIS /* maybe missing LUW */
#define SQL_IBMi_FMT_JIS 4
#endif
#ifndef SQL_IBMi_FMT_MDY /* maybe missing LUW */
#define SQL_IBMi_FMT_MDY 5
#endif
#ifndef SQL_IBMi_FMT_DMY /* maybe missing LUW */
#define SQL_IBMi_FMT_DMY 6
#endif
#ifndef SQL_IBMi_FMT_YMD /* maybe missing LUW */
#define SQL_IBMi_FMT_YMD 7
#endif
#ifndef SQL_IBMi_FMT_JUL /* maybe missing LUW */
#define SQL_IBMi_FMT_JUL 8
#endif
#ifndef SQL_IBMi_FMT_HMS /* maybe missing LUW */
#define SQL_IBMi_FMT_HMS 9
#endif
#ifndef SQL_IBMi_FMT_JOB /* maybe missing LUW */
#define SQL_IBMi_FMT_JOB 10
#endif

#ifndef SQL_FMT_ISO
#define SQL_FMT_ISO SQL_IBMi_FMT_ISO
#endif
#ifndef SQL_FMT_USA
#define SQL_FMT_USA SQL_IBMi_FMT_USA
#endif
#ifndef SQL_FMT_EUR
#define SQL_FMT_EUR SQL_IBMi_FMT_EUR
#endif
#ifndef SQL_FMT_JIS
#define SQL_FMT_JIS SQL_IBMi_FMT_JIS
#endif
#ifndef SQL_FMT_DMY
#define SQL_FMT_DMY SQL_IBMi_FMT_MDY
#endif
#ifndef SQL_FMT_MDY
#define SQL_FMT_MDY SQL_IBMi_FMT_DMY
#endif
#ifndef SQL_FMT_YMD
#define SQL_FMT_YMD SQL_IBMi_FMT_YMD
#endif
#ifndef SQL_FMT_JUL
#define SQL_FMT_JUL SQL_IBMi_FMT_JUL
#endif
#ifndef SQL_FMT_JOB
#define SQL_FMT_JOB SQL_IBMi_FMT_JOB
#endif
#ifndef SQL_FMT_HMS
#define SQL_FMT_HMS SQL_IBMi_FMT_HMS
#endif


/* connect date and time separator */
#ifndef SQL_ATTR_DATE_SEP /* maybe missing LUW */
#define SQL_ATTR_DATE_SEP 3026 
#endif
#ifndef SQL_ATTR_TIME_SEP /* maybe missing LUW */
#define SQL_ATTR_TIME_SEP 3028 
#endif
#ifndef SQL_ATTR_DECIMAL_SEP /* maybe missing LUW */
#define SQL_ATTR_DECIMAL_SEP 3029 
#endif
#ifndef SQL_SEP_SLASH /* maybe missing LUW */
#define SQL_SEP_SLASH 1
#endif
#ifndef SQL_SEP_DASH /* maybe missing LUW */
#define SQL_SEP_DASH 2
#endif
#ifndef SQL_SEP_PERIOD /* maybe missing LUW */
#define SQL_SEP_PERIOD 3
#endif
#ifndef SQL_SEP_COMMA /* maybe missing LUW */
#define SQL_SEP_COMMA 4
#endif
#ifndef SQL_SEP_BLANK /* maybe missing LUW */
#define SQL_SEP_BLANK 5
#endif
#ifndef SQL_SEP_COLON /* maybe missing LUW */
#define SQL_SEP_COLON 6
#endif
#ifndef SQL_SEP_JOB /* maybe missing LUW */
#define SQL_SEP_JOB 7
#endif

/* connect query goal */
#ifndef SQL_ATTR_QUERY_OPTIMIZE_GOAL /* maybe missing LUW */
#define SQL_ATTR_QUERY_OPTIMIZE_GOAL SQL_ATTR_MISSING
#endif
#ifndef SQL_FIRST_IO /* maybe missing LUW */
#define SQL_FIRST_IO SQL_TYPE_MISSING2
#endif
#ifndef SQL_ALL_IO /* maybe missing LUW */
#define SQL_ALL_IO SQL_TYPE_MISSING3
#endif

/* statement attributes */
#ifndef SQL_ATTR_FOR_FETCH_ONLY  /* maybe missing LUW */
#define SQL_ATTR_FOR_FETCH_ONLY SQL_ATTR_MISSING
#endif

/* statement concurrency */
#ifndef SQL_ATTR_CONCURRENCY  /* maybe missing LUW */
#define SQL_ATTR_CONCURRENCY SQL_CONCURRENCY
#endif

/* needed for backward compatibility (SQL_ATTR_ROWCOUNT_PREFETCH not defined prior to DB2 9.5.0.3) */
#ifndef SQL_ATTR_ROWCOUNT_PREFETCH
#define SQL_ATTR_ROWCOUNT_PREFETCH 2592
#define SQL_ROWCOUNT_PREFETCH_OFF   0
#define SQL_ROWCOUNT_PREFETCH_ON    1
#endif

#ifndef SQL_ATTR_INFO_USERID
#define SQL_ATTR_INFO_USERID 1281
#define SQL_ATTR_INFO_WRKSTNNAME 1282
#define SQL_ATTR_INFO_APPLNAME 1283
#define SQL_ATTR_INFO_ACCTSTR 1284
#define SQL_ATTR_INFO_PROGRAMID 2511
#endif

/* SQL_ATTR_USE_TRUSTED_CONTEXT, 
 * SQL_ATTR_TRUSTED_CONTEXT_USERID and 
 * SQL_ATTR_TRUSTED_CONTEXT_PASSWORD
 * not defined prior to DB2 v9 */
#ifndef SQL_ATTR_USE_TRUSTED_CONTEXT
#define SQL_ATTR_USE_TRUSTED_CONTEXT 2561
#define SQL_ATTR_TRUSTED_CONTEXT_USERID 2562
#define SQL_ATTR_TRUSTED_CONTEXT_PASSWORD 2563
#endif

#ifndef SQL_ATTR_REPLACE_QUOTED_LITERALS
#define SQL_ATTR_REPLACE_QUOTED_LITERALS 2586
#endif

/* CLI v9.1 FP3 and below has a SQL_ATTR_REPLACE_QUOTED_LITERALS value of 116
 * We need to support both the new and old values for compatibility with older
 * versions of CLI. CLI v9.1 FP4 and beyond changed this value to 2586
 */
#define SQL_ATTR_REPLACE_QUOTED_LITERALS_OLDVALUE 116

/* If using a DB2 CLI version which doesn't support this functionality, explicitly
 * define this. We will rely on DB2 CLI to throw an error when SQLGetStmtAttr is 
 * called.
 */
#ifndef SQL_ATTR_GET_GENERATED_VALUE 
#define SQL_ATTR_GET_GENERATED_VALUE 2578
#endif

#endif /* not PASE */
#endif /* RUBY_SQL_LUW_H */

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 */

