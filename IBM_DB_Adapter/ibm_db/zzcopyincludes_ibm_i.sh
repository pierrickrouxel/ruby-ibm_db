#!/QOpenSys/usr/bin/ksh
ls /QIBM/include/sql* > ./list2
for i in $(< ./list2)
do
  echo "PREPARATION COPY"
  system -v "CPY OBJ('$i') TODIR('/usr/include/') TOCCSID(*STDASCII) DTAFMT(*TEXT) REPLACE(*YES)"                                      
done
rm list2

#LUW uses sqlcli1.h (everywhere), so we add that include to PASE as follows.
printf "#include \"sql.h\"\n#include \"sqlca.h\"\n#include \"sqlcli.h\"\n" > /usr/include/sqlcli1.h 

