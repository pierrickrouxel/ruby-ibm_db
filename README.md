#ruby-ibm_db
Rails Adapter/Ruby Driver for IBM i (fork)

#Contributors
- Tony Cairns, IBM
- Aaron Bartell, KrengelTech

Build and test: IBM_DB_Adapter/ibm_db/README_IBM_i 

Yips: http://youngiprofessionals.com/wiki/index.php/XMLSERVICE/Ruby

#Install
##Overwrite /PowerRuby
Note, it would be best to make a backup of /PowerRuby folder before doing the below.  Also, you can download the gem via FTP and upload to IBM i via FTP if you don't have `curl`.
```
$ curl -OkL https://bitbucket.org/litmis/ruby-ibm_db/downloads/ibm_db-2.5.14-powerpc-aix-6_v7.1_prV20_GA05.gem
$ gem uninstall ibm_db
$ gem install ./ibm_db-2.5.14-powerpc-aix-6_v7.1_prV20_GA05.gem
```

##Alternate Install Location
Preferred approach because installing in /PowerRuby will get overwritten with /PowerRuby upgrade and there's no way to revert to go back to original /PowerRuby version.

Create the alternate directory where the `ibm_db` gem will be installed and then alter `GEM_HOME` and `GEM_PATH` accordingly.
```
$ mkdir -p /ruby/gemsets/ruby-ibm_db
$ export GEM_HOME=/ruby/gemsets/ruby-ibm_db
$ export GEM_PATH=/ruby/gemsets/ruby-ibm_db:/PowerRuby/prV2R0/lib/ruby/gems/2.0.0
$ curl -OkL https://bitbucket.org/litmis/ruby-ibm_db/downloads/ibm_db-2.5.14-powerpc-aix-6_v7.1_prV20_GA05.gem
$ gem install ./ibm_db-2.5.14-powerpc-aix-6_v7.1_prV20_GA05.gem
```

#Build and Compile
Assumes: http://yips.idevcloud.com/wiki/index.php/PASE/GCC
**Note:** Must be built on IBM i v7.1 in order to work on v7.1 and v7.2.
**Note:** This is only necessary if you are looking to actually *build* the driver with a C compiler.  If you just want to install the precompiled driver then go to the **Install** section in this document.

Best practice is to build/compile into directory other than `/PowerRuby`.  File `zzall_ibm_i.sh` accomplishes a different install location by default (i.e. `/ruby/gemsets/ruby-ibm_db`).

```
$ export PATH=/PowerRuby/prV2R0/bin:$PATH
$ export LIBPATH=/PowerRuby/prV2R0/lib
$ git clone git@bitbucket.org:litmis/ruby-ibm_db.git
$ cd ruby-ibm_db/IBM_DB_Adapter/ibm_db/
$ ./zzall_ibm_i.sh
```

There will now be two new files: 

- `ibm_db-2.5.14.gem` - Gem created with 'gem build ...'
- `ibm_db-2.5.14-powerpc-aix-6.gem` - "Fat gem" created with `gem compile ...` that includes C binaries.  This file is meant to be renamed to have the OS version and PowerRuby version included and is meant for distribution. i.e. `ibm_db-2.5.14-powerpc-aix-6_v7.1_prV20.gem`.

#Distribution
Increment `ruby-ibm_db/IBM_DB_Adapter/ibm_db/version.txt`.  Do NOT change the version of the actual gem itself.

Rename final distributable to include IBM i OS version and version of PowerRuby this was compiled with.
```
$ mv ibm_db-2.5.14-powerpc-aix-6.gem ibm_db-2.5.14-powerpc-aix-6_v7.1_prV20.gem
```

Upload to https://bitbucket.org/litmis/ruby-ibm_db/downloads

#Test
```
$ export PATH=/PowerRuby/prV2R0/bin:$PATH
$ export LIBPATH=/PowerRuby/prV2R0/lib
$ git clone git@bitbucket.org:litmis/ruby-ibm_db.git
$ cd ruby-ibm_db/IBM_DB_Adapter/ibm_db/test/IBMi/
$ rake
```